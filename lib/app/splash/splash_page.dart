import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../app_controller.dart';
import 'splash_controller.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends ModularState<SplashPage, SplashController> {
  //use 'controller' variable to access controller

  final appController = Modular.get<AppController>();

  @override
  void initState() {
    super.initState();
    Modular.get<SplashController>().inicial();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Observer(builder: (_) {
        return !appController.error_connect
            ? CircularProgressIndicator()
            : Container(
                color: Colors.white,
                height: 220,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(32),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  elevation: 16,
                  color: Colors.lightBlueAccent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: Colors.yellow,
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Não foi possivel conectar com a internet",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      RaisedButton(
                        color: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: Text(
                          "Ok",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          Modular.get<SplashController>().inicial();
                        },
                      )
                    ],
                  ),
                ),
              );
      }),
    );
  }
}
