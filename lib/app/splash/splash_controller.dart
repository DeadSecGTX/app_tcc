import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import '../modules/shared/validate/connect_validate.dart';

part 'splash_controller.g.dart';

class SplashController = _SplashController with _$SplashController;

abstract class _SplashController with Store {
  Future<void> inicial() async {
    bool a = await Modular.get<ConnectValidate>().isConnected();
    if (a) {
      await LocalStorage.getValue<int>('nivel').then((value) {
        print(value);
        if (value == 1) {
          Modular.to.pushNamed('/home/');
          return;
        } else if (value == 2) {
          Modular.to.pushNamed('/adm/');
          return;
        } else {
          Modular.to.pushNamed('/login/');
        }
      });
    }
  }
}
