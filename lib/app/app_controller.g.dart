// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppController on _AppControllerBase, Store {
  final _$error_connectAtom = Atom(name: '_AppControllerBase.error_connect');

  @override
  bool get error_connect {
    _$error_connectAtom.reportRead();
    return super.error_connect;
  }

  @override
  set error_connect(bool value) {
    _$error_connectAtom.reportWrite(value, super.error_connect, () {
      super.error_connect = value;
    });
  }

  final _$error_generalAtom = Atom(name: '_AppControllerBase.error_general');

  @override
  bool get error_general {
    _$error_generalAtom.reportRead();
    return super.error_general;
  }

  @override
  set error_general(bool value) {
    _$error_generalAtom.reportWrite(value, super.error_general, () {
      super.error_general = value;
    });
  }

  final _$msgAtom = Atom(name: '_AppControllerBase.msg');

  @override
  String get msg {
    _$msgAtom.reportRead();
    return super.msg;
  }

  @override
  set msg(String value) {
    _$msgAtom.reportWrite(value, super.msg, () {
      super.msg = value;
    });
  }

  @override
  String toString() {
    return '''
error_connect: ${error_connect},
error_general: ${error_general},
msg: ${msg}
    ''';
  }
}
