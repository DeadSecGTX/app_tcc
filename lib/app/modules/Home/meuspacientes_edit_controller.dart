import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/shared/repositories/home_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
part 'meuspacientes_edit_controller.g.dart';

class MeusPacientesEditController = _MeusPacientesEditControllerBase
    with _$MeusPacientesEditController;

abstract class _MeusPacientesEditControllerBase with Store {
  final HomeRepository repository;
  final appController = Modular.get<AppController>();

  _MeusPacientesEditControllerBase(this.repository);

  String references,
      nomeO,
      idadeO,
      sexoO,
      tempO,
      pressaoO,
      riscoO,
      localO,
      examesO,
      medicamentoO;

  @observable
  String nome = "";

  @action
  void setEmail(String value) => email = value;

  @action
  void setNome(String value) => nome = value;

  @observable
  String idade = "";

  @action
  void setIdade(String value) => idade = value;

  @observable
  String sexo = "";

  @action
  void setSexo(String value) => sexo = value;

  @observable
  String temp = "";

  @action
  void setTemp(String value) => temp = value;

  @observable
  String pressao = "";

  @action
  void setPressao(String value) => pressao = value;

  @observable
  String risco = "";

  @action
  void setRisco(String value) => risco = value;

  @observable
  String local = "";

  @action
  void setLocal(String value) => local = value;

  @observable
  String exames = "";

  @action
  void setExames(String value) => exames = value;

  @observable
  String medicamentos = "";

  @action
  void setMedicamentos(String value) => medicamentos = value;

  @computed
  bool get isNomeChange => this.nome != nomeO;

  @computed
  bool get isIdadeChange => this.idade != idadeO;

  @computed
  bool get isSexoChange => this.sexo != sexoO;

  @computed
  bool get isPressaoChange => this.pressao != pressaoO;

  @computed
  bool get isTempChange => this.temp != tempO;

  @computed
  bool get isRiscoChange => this.risco != riscoO;

  @computed
  bool get isLocalChange => this.local != localO;

  @computed
  bool get isExameChange => this.exames != examesO;

  @computed
  bool get isMedicChange => this.medicamentos != medicamentoO;

  @observable
  bool botaoAlterar = false;

  @observable
  bool botaoBaixa = false;

  @observable
  bool botaoDesassociar = false;

  @observable
  bool botaoHL7 = false;

  @observable
  String email = "";

  @observable
  bool verifyEmail = false;

  String validarNome(String nome) {
    if (nome == null || nome.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (nome.length < 5) {
      return "Nome invalido";
    }

    return null;
  }

  String validarIdade(String idade) {
    if (idade == null || idade.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (idade.length < 1) {
      return "A idade invalida";
    }

    return null;
  }

  String validarSexo(String sexo) {
    if (sexo == null || sexo.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (sexo.length < 3) {
      return "Sexo invalido";
    }

    return null;
  }

  String validarTemp(String temp) {
    if (temp == null || temp.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (temp.length < 1) {
      return "Temperatura invalida";
    }

    return null;
  }

  String validarPressao(String pressao) {
    if (pressao == null || pressao.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (pressao.length < 1) {
      return "Pressão invalida";
    }

    return null;
  }

  String validarRisco(String risco) {
    if (risco != "nao-urgente" &&
        risco != "pouco-urgente" &&
        risco != "urgente" &&
        risco != "muita-urgencia" &&
        risco != "emergencia") {
      return "Classificação invalida";
    } else if (risco == null || risco.isEmpty) {
      return "Este campo é obrigatorio";
    }

    return null;
  }

  String validarLocal(String local) {
    if (local == null || local.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (local.length < 1) {
      return "Local invalido";
    }

    return null;
  }

  String validarExames(String exames) {
    if (exames == null || exames.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (exames.length < 1) {
      return "Exame invalido";
    }

    return null;
  }

  String validarMedicamentos(String medicamentos) {
    if (medicamentos == null || medicamentos.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (medicamentos.length < 1) {
      return "Medicamento invalido";
    }

    return null;
  }

  @computed
  Function get savePressed => (isNomeChange ||
          isIdadeChange ||
          isSexoChange ||
          isPressaoChange ||
          isTempChange ||
          isRiscoChange ||
          isLocalChange ||
          isExameChange ||
          isMedicChange)
      ? clicaBotao
      : null;

  @action
  clicaBotao() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoAlterar = true;
    }
  }

  @action
  clicaBotaoBaixa() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoBaixa = true;
    }
  }

  @action
  clicaBotaoDes() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoDesassociar = true;
    }
  }

  @action
  clicaBotaoHL7() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoHL7 = true;
    }
  }

  @action
  desassociar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      try {
        await repository.update(references, "0");
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao desassociar paciente";
        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  alterar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    print(medicamentos);
    print(exames);
    if (teste == true) {
      try {
        print("Alterar exame: " + exames);
        await repository.updatePacientes(references, nome, idade, sexo, pressao,
            temp, risco, local, exames, medicamentos);
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao alterar dados do paciente";
        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  baixa() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      try {
        await repository.baixa(references);
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao dar baixa";

        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  hl7() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      try {
        print("teste passagem" + email);
        bool ei = await repository.listaVerify(email);
        if (ei) {
          await repository.hl7(references, email);
          return;
        } else {
          bool testeVerefy = await repository.verify(email);
          if (testeVerefy) {
            verifyEmail = true;
            return;
          } else {
            appController.error_general = true;
            appController.msg = "Email invalido";
            return;
          }
        }
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao enviar hl7";

        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  passartela(
    String references,
    String nome,
    String idade,
    String sexo,
    String pressao,
    String temp,
    String risco,
    String local,
    String exame,
    String medic,
  ) {
    this.references = references;
    this.nome = nome;
    this.idade = idade;
    this.sexo = sexo;
    this.pressao = pressao;
    this.temp = temp;
    this.risco = risco;
    this.local = local;
    this.exames = exame;
    this.medicamentos = medic;

    this.nomeO = nome;
    this.idadeO = idade;
    this.sexoO = sexo;
    this.pressaoO = pressao;
    this.tempO = temp;
    this.riscoO = risco;
    this.localO = local;
    this.examesO = exames;
    this.medicamentoO = medicamentos;

    Modular.to.pushNamed('/editpacientes');
  }
}
