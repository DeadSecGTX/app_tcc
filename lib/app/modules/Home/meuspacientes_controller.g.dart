// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meuspacientes_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MeusPacientesController on _MeusPacientesControllerBase, Store {
  final _$pacienteListAtom =
      Atom(name: '_MeusPacientesControllerBase.pacienteList');

  @override
  ObservableStream<List<Paciente>> get pacienteList {
    _$pacienteListAtom.reportRead();
    return super.pacienteList;
  }

  @override
  set pacienteList(ObservableStream<List<Paciente>> value) {
    _$pacienteListAtom.reportWrite(value, super.pacienteList, () {
      super.pacienteList = value;
    });
  }

  final _$nomeAtom = Atom(name: '_MeusPacientesControllerBase.nome');

  @override
  String get nome {
    _$nomeAtom.reportRead();
    return super.nome;
  }

  @override
  set nome(String value) {
    _$nomeAtom.reportWrite(value, super.nome, () {
      super.nome = value;
    });
  }

  final _$_MeusPacientesControllerBaseActionController =
      ActionController(name: '_MeusPacientesControllerBase');

  @override
  dynamic getMeusPacientesList() {
    final _$actionInfo = _$_MeusPacientesControllerBaseActionController
        .startAction(name: '_MeusPacientesControllerBase.getMeusPacientesList');
    try {
      return super.getMeusPacientesList();
    } finally {
      _$_MeusPacientesControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
pacienteList: ${pacienteList},
nome: ${nome}
    ''';
  }
}
