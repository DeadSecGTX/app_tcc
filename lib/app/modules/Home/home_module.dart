import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/Home/meuspacientes_controller.dart';
import 'package:app_tcc/app/modules/Home/meuspacientes_edit_controller.dart';
import 'package:app_tcc/app/modules/Home/pages/home_meuspacientes_edit_page.dart';
import 'package:app_tcc/app/modules/Home/pages/home_meuspacientes_page.dart';
import 'package:app_tcc/app/modules/Home/pages/home_page.dart';
import 'package:app_tcc/app/modules/shared/repositories/home_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_controller.dart';

class HomeModule extends ChildModule {
  @override
  // TODO: implement binds
  List<Bind> get binds => [
        Bind((i) => HomeController(i.get())),
        Bind((i) => MeusPacientesController(i.get())),
        Bind((i) => HomeRepository(FirebaseFirestore.instance)),
        Bind((i) => AppController()),
        Bind((i) => ConnectValidate()),
        Bind((i) => MeusPacientesEditController(i.get())),
      ];

  @override
  // TODO: implement routers
  List<Router> get routers => [
        Router('/', child: (_, args) => HomePage()),
        Router('/meuspacientes', child: (_, args) => HomeMeusPacientes()),
        Router('/editpacientes', child: (_, args) => MeusPacientesEditPage())
      ];

  static Inject get to => Inject<HomeModule>.of();
}
