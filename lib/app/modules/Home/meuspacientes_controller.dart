import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/Home/home_controller.dart';
import 'package:app_tcc/app/modules/models/paciente_model.dart';
import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:app_tcc/app/modules/shared/repositories/home_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
part 'meuspacientes_controller.g.dart';

class MeusPacientesController = _MeusPacientesControllerBase
    with _$MeusPacientesController;

abstract class _MeusPacientesControllerBase with Store {
  final HomeRepository repository;
  final appController = Modular.get<AppController>();
  final homeController = Modular.get<HomeController>();

  _MeusPacientesControllerBase(this.repository) {
    getMeusPacientesList();
  }

  @observable
  ObservableStream<List<Paciente>> pacienteList;

  @observable
  String nome = "";

  String id = "";

  getNomeShered() async {
    await LocalStorage.getValue<String>('nome').then((value) {
      nome = value;
      print("nome shered: " + nome);
    });
  }

  @action
  getMeusPacientesList() {
    pacienteList =
        repository.listMeusPacientes(homeController.id).asObservable();
  }
}
