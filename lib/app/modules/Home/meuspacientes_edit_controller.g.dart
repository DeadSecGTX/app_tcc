// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meuspacientes_edit_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MeusPacientesEditController on _MeusPacientesEditControllerBase, Store {
  Computed<bool> _$isNomeChangeComputed;

  @override
  bool get isNomeChange =>
      (_$isNomeChangeComputed ??= Computed<bool>(() => super.isNomeChange,
              name: '_MeusPacientesEditControllerBase.isNomeChange'))
          .value;
  Computed<bool> _$isIdadeChangeComputed;

  @override
  bool get isIdadeChange =>
      (_$isIdadeChangeComputed ??= Computed<bool>(() => super.isIdadeChange,
              name: '_MeusPacientesEditControllerBase.isIdadeChange'))
          .value;
  Computed<bool> _$isSexoChangeComputed;

  @override
  bool get isSexoChange =>
      (_$isSexoChangeComputed ??= Computed<bool>(() => super.isSexoChange,
              name: '_MeusPacientesEditControllerBase.isSexoChange'))
          .value;
  Computed<bool> _$isPressaoChangeComputed;

  @override
  bool get isPressaoChange =>
      (_$isPressaoChangeComputed ??= Computed<bool>(() => super.isPressaoChange,
              name: '_MeusPacientesEditControllerBase.isPressaoChange'))
          .value;
  Computed<bool> _$isTempChangeComputed;

  @override
  bool get isTempChange =>
      (_$isTempChangeComputed ??= Computed<bool>(() => super.isTempChange,
              name: '_MeusPacientesEditControllerBase.isTempChange'))
          .value;
  Computed<bool> _$isRiscoChangeComputed;

  @override
  bool get isRiscoChange =>
      (_$isRiscoChangeComputed ??= Computed<bool>(() => super.isRiscoChange,
              name: '_MeusPacientesEditControllerBase.isRiscoChange'))
          .value;
  Computed<bool> _$isLocalChangeComputed;

  @override
  bool get isLocalChange =>
      (_$isLocalChangeComputed ??= Computed<bool>(() => super.isLocalChange,
              name: '_MeusPacientesEditControllerBase.isLocalChange'))
          .value;
  Computed<bool> _$isExameChangeComputed;

  @override
  bool get isExameChange =>
      (_$isExameChangeComputed ??= Computed<bool>(() => super.isExameChange,
              name: '_MeusPacientesEditControllerBase.isExameChange'))
          .value;
  Computed<bool> _$isMedicChangeComputed;

  @override
  bool get isMedicChange =>
      (_$isMedicChangeComputed ??= Computed<bool>(() => super.isMedicChange,
              name: '_MeusPacientesEditControllerBase.isMedicChange'))
          .value;
  Computed<Function> _$savePressedComputed;

  @override
  Function get savePressed =>
      (_$savePressedComputed ??= Computed<Function>(() => super.savePressed,
              name: '_MeusPacientesEditControllerBase.savePressed'))
          .value;

  final _$nomeAtom = Atom(name: '_MeusPacientesEditControllerBase.nome');

  @override
  String get nome {
    _$nomeAtom.reportRead();
    return super.nome;
  }

  @override
  set nome(String value) {
    _$nomeAtom.reportWrite(value, super.nome, () {
      super.nome = value;
    });
  }

  final _$idadeAtom = Atom(name: '_MeusPacientesEditControllerBase.idade');

  @override
  String get idade {
    _$idadeAtom.reportRead();
    return super.idade;
  }

  @override
  set idade(String value) {
    _$idadeAtom.reportWrite(value, super.idade, () {
      super.idade = value;
    });
  }

  final _$sexoAtom = Atom(name: '_MeusPacientesEditControllerBase.sexo');

  @override
  String get sexo {
    _$sexoAtom.reportRead();
    return super.sexo;
  }

  @override
  set sexo(String value) {
    _$sexoAtom.reportWrite(value, super.sexo, () {
      super.sexo = value;
    });
  }

  final _$tempAtom = Atom(name: '_MeusPacientesEditControllerBase.temp');

  @override
  String get temp {
    _$tempAtom.reportRead();
    return super.temp;
  }

  @override
  set temp(String value) {
    _$tempAtom.reportWrite(value, super.temp, () {
      super.temp = value;
    });
  }

  final _$pressaoAtom = Atom(name: '_MeusPacientesEditControllerBase.pressao');

  @override
  String get pressao {
    _$pressaoAtom.reportRead();
    return super.pressao;
  }

  @override
  set pressao(String value) {
    _$pressaoAtom.reportWrite(value, super.pressao, () {
      super.pressao = value;
    });
  }

  final _$riscoAtom = Atom(name: '_MeusPacientesEditControllerBase.risco');

  @override
  String get risco {
    _$riscoAtom.reportRead();
    return super.risco;
  }

  @override
  set risco(String value) {
    _$riscoAtom.reportWrite(value, super.risco, () {
      super.risco = value;
    });
  }

  final _$localAtom = Atom(name: '_MeusPacientesEditControllerBase.local');

  @override
  String get local {
    _$localAtom.reportRead();
    return super.local;
  }

  @override
  set local(String value) {
    _$localAtom.reportWrite(value, super.local, () {
      super.local = value;
    });
  }

  final _$examesAtom = Atom(name: '_MeusPacientesEditControllerBase.exames');

  @override
  String get exames {
    _$examesAtom.reportRead();
    return super.exames;
  }

  @override
  set exames(String value) {
    _$examesAtom.reportWrite(value, super.exames, () {
      super.exames = value;
    });
  }

  final _$medicamentosAtom =
      Atom(name: '_MeusPacientesEditControllerBase.medicamentos');

  @override
  String get medicamentos {
    _$medicamentosAtom.reportRead();
    return super.medicamentos;
  }

  @override
  set medicamentos(String value) {
    _$medicamentosAtom.reportWrite(value, super.medicamentos, () {
      super.medicamentos = value;
    });
  }

  final _$botaoAlterarAtom =
      Atom(name: '_MeusPacientesEditControllerBase.botaoAlterar');

  @override
  bool get botaoAlterar {
    _$botaoAlterarAtom.reportRead();
    return super.botaoAlterar;
  }

  @override
  set botaoAlterar(bool value) {
    _$botaoAlterarAtom.reportWrite(value, super.botaoAlterar, () {
      super.botaoAlterar = value;
    });
  }

  final _$botaoBaixaAtom =
      Atom(name: '_MeusPacientesEditControllerBase.botaoBaixa');

  @override
  bool get botaoBaixa {
    _$botaoBaixaAtom.reportRead();
    return super.botaoBaixa;
  }

  @override
  set botaoBaixa(bool value) {
    _$botaoBaixaAtom.reportWrite(value, super.botaoBaixa, () {
      super.botaoBaixa = value;
    });
  }

  final _$botaoDesassociarAtom =
      Atom(name: '_MeusPacientesEditControllerBase.botaoDesassociar');

  @override
  bool get botaoDesassociar {
    _$botaoDesassociarAtom.reportRead();
    return super.botaoDesassociar;
  }

  @override
  set botaoDesassociar(bool value) {
    _$botaoDesassociarAtom.reportWrite(value, super.botaoDesassociar, () {
      super.botaoDesassociar = value;
    });
  }

  final _$botaoHL7Atom =
      Atom(name: '_MeusPacientesEditControllerBase.botaoHL7');

  @override
  bool get botaoHL7 {
    _$botaoHL7Atom.reportRead();
    return super.botaoHL7;
  }

  @override
  set botaoHL7(bool value) {
    _$botaoHL7Atom.reportWrite(value, super.botaoHL7, () {
      super.botaoHL7 = value;
    });
  }

  final _$emailAtom = Atom(name: '_MeusPacientesEditControllerBase.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$verifyEmailAtom =
      Atom(name: '_MeusPacientesEditControllerBase.verifyEmail');

  @override
  bool get verifyEmail {
    _$verifyEmailAtom.reportRead();
    return super.verifyEmail;
  }

  @override
  set verifyEmail(bool value) {
    _$verifyEmailAtom.reportWrite(value, super.verifyEmail, () {
      super.verifyEmail = value;
    });
  }

  final _$clicaBotaoAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.clicaBotao');

  @override
  Future clicaBotao() {
    return _$clicaBotaoAsyncAction.run(() => super.clicaBotao());
  }

  final _$clicaBotaoBaixaAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.clicaBotaoBaixa');

  @override
  Future clicaBotaoBaixa() {
    return _$clicaBotaoBaixaAsyncAction.run(() => super.clicaBotaoBaixa());
  }

  final _$clicaBotaoDesAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.clicaBotaoDes');

  @override
  Future clicaBotaoDes() {
    return _$clicaBotaoDesAsyncAction.run(() => super.clicaBotaoDes());
  }

  final _$clicaBotaoHL7AsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.clicaBotaoHL7');

  @override
  Future clicaBotaoHL7() {
    return _$clicaBotaoHL7AsyncAction.run(() => super.clicaBotaoHL7());
  }

  final _$desassociarAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.desassociar');

  @override
  Future desassociar() {
    return _$desassociarAsyncAction.run(() => super.desassociar());
  }

  final _$alterarAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.alterar');

  @override
  Future alterar() {
    return _$alterarAsyncAction.run(() => super.alterar());
  }

  final _$baixaAsyncAction =
      AsyncAction('_MeusPacientesEditControllerBase.baixa');

  @override
  Future baixa() {
    return _$baixaAsyncAction.run(() => super.baixa());
  }

  final _$hl7AsyncAction = AsyncAction('_MeusPacientesEditControllerBase.hl7');

  @override
  Future hl7() {
    return _$hl7AsyncAction.run(() => super.hl7());
  }

  final _$_MeusPacientesEditControllerBaseActionController =
      ActionController(name: '_MeusPacientesEditControllerBase');

  @override
  void setEmail(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setEmail');
    try {
      return super.setEmail(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setNome(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setNome');
    try {
      return super.setNome(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setIdade(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setIdade');
    try {
      return super.setIdade(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setSexo(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setSexo');
    try {
      return super.setSexo(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setTemp(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setTemp');
    try {
      return super.setTemp(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setPressao(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setPressao');
    try {
      return super.setPressao(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setRisco(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setRisco');
    try {
      return super.setRisco(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setLocal(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setLocal');
    try {
      return super.setLocal(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setExames(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setExames');
    try {
      return super.setExames(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  void setMedicamentos(String value) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.setMedicamentos');
    try {
      return super.setMedicamentos(value);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  dynamic passartela(
      String references,
      String nome,
      String idade,
      String sexo,
      String pressao,
      String temp,
      String risco,
      String local,
      String exame,
      String medic) {
    final _$actionInfo = _$_MeusPacientesEditControllerBaseActionController
        .startAction(name: '_MeusPacientesEditControllerBase.passartela');
    try {
      return super.passartela(references, nome, idade, sexo, pressao, temp,
          risco, local, exame, medic);
    } finally {
      _$_MeusPacientesEditControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
nome: ${nome},
idade: ${idade},
sexo: ${sexo},
temp: ${temp},
pressao: ${pressao},
risco: ${risco},
local: ${local},
exames: ${exames},
medicamentos: ${medicamentos},
botaoAlterar: ${botaoAlterar},
botaoBaixa: ${botaoBaixa},
botaoDesassociar: ${botaoDesassociar},
botaoHL7: ${botaoHL7},
email: ${email},
verifyEmail: ${verifyEmail},
isNomeChange: ${isNomeChange},
isIdadeChange: ${isIdadeChange},
isSexoChange: ${isSexoChange},
isPressaoChange: ${isPressaoChange},
isTempChange: ${isTempChange},
isRiscoChange: ${isRiscoChange},
isLocalChange: ${isLocalChange},
isExameChange: ${isExameChange},
isMedicChange: ${isMedicChange},
savePressed: ${savePressed}
    ''';
  }
}
