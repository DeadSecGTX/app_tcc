import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/Home/home_controller.dart';
import 'package:app_tcc/app/modules/Home/meuspacientes_controller.dart';
import 'package:app_tcc/app/modules/models/paciente_model.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/widgets/PacientesNovo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final appController = Modular.get<AppController>();
  final homeController = Modular.get<HomeController>();
  final meuspacientesController = Modular.get<MeusPacientesController>();

  @override
  void initState() {
    super.initState();
    homeController.getNomeShered();
    homeController.getCodigoShered();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      await homeController.removeValuesShered();
      Modular.to.pushNamed('/login/');
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Observer(builder: (_) {
        return Scaffold(
          appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
              backgroundColor: Colors.white,
              title: Text(
                homeController.nome,
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
              actions: <Widget>[
                Center(
                  child: SizedBox(
                    width: 130,
                    //height: 10,
                    child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        color: Colors.blueAccent,
                        child: Text(
                          "Meus Pacientes",
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                        onPressed: () async {
                          await meuspacientesController.getMeusPacientesList();
                          Modular.to.pushNamed('/meuspacientes');
                        }),
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.close,
                    size: 30,
                    color: Colors.red,
                  ),
                  onPressed: () async {
                    await homeController.removeValuesShered();
                    Modular.to.pushNamed('/login/');
                  },
                )
              ]),
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                stops: [0.1, 1],
                colors: [
                  Colors.blue[500],
                  Colors.lightBlue[100],
                ],
              ),
            ),
            child: Observer(
              builder: (_) {
                if (homeController.pacienteList.hasError) {
                  return Center(
                    child: RaisedButton(
                      color: Colors.greenAccent,
                      onPressed: homeController.getList,
                      child: Text("Recarregar.."),
                    ),
                  );
                }
                if (homeController.pacienteList.data == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                List<Paciente> list = homeController.pacienteList.data;

                return ListView.builder(
                    itemCount: list.length,
                    itemBuilder: (_, index) {
                      Color c;
                      Paciente p = list[index];
                      switch (p.risco) {
                        case "nao-urgente":
                          c = Color(0xFF7D79D0);
                          break;
                        case "pouco-urgente":
                          c = Colors.greenAccent;
                          break;
                        case "urgente":
                          c = Colors.yellowAccent;
                          break;
                        case "muito-urgente":
                          c = Colors.orangeAccent;
                          break;
                        case "emergencia":
                          c = Colors.redAccent;
                          break;
                        default:
                          c = Color(0xFF7D79D0);
                      }
                      return p.user_id == "0"
                          ? Padding(
                              padding: EdgeInsets.only(
                                  top: 10, left: 15, right: 15, bottom: 5),
                              child: Container(
                                  height: 100,
                                  //margin: EdgeInsets.only(bottom: 10),
                                  decoration: BoxDecoration(
                                    color: c,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(16)),
                                  ),
                                  child: GestureDetector(
                                    child: PacientesNovoWidget(
                                      nome: p.nome,
                                      risco: p.risco,
                                      status: false,
                                    ),
                                    onDoubleTap: () async {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return ResponseError(
                                              icon: Icons.warning,
                                              mensagem: "Paciente selecionado!",
                                              funct: homeController
                                                  .adicionaMeusPacientes(
                                                      p.key));
                                        },
                                      );
                                    },
                                  )),
                            )
                          : Container();
                    });
              },
            ),
          ),
        );
      }),
    );
  }
}
