import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/Home/meuspacientes_controller.dart';
import 'package:app_tcc/app/modules/Home/meuspacientes_edit_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class MeusPacientesEditPage extends StatefulWidget {
  @override
  _MeusPacientesEditPageState createState() => _MeusPacientesEditPageState();
}

class _MeusPacientesEditPageState extends State<MeusPacientesEditPage> {
  final appController = Modular.get<AppController>();
  final meuspacientesController = Modular.get<MeusPacientesController>();
  final meusPacientesEditController =
      Modular.get<MeusPacientesEditController>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    //Erros diversos nos web services
    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });

    //Confirmar alteração
    reaction((_) => meusPacientesEditController.botaoAlterar, (botaoAlterar) {
      if (botaoAlterar == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(Icons.warning),
              content: Text("Deseja alterar esses dados ?"),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await meusPacientesEditController.alterar();
                        meusPacientesEditController.botaoAlterar = false;
                        await meuspacientesController.getMeusPacientesList();
                        Navigator.pop(context);
                        Modular.to.pushNamed('/meuspacientes');
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        meusPacientesEditController.botaoAlterar = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });

    reaction((_) => meusPacientesEditController.botaoDesassociar,
        (botaoDesassociar) {
      if (botaoDesassociar == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(Icons.warning),
              content: Text("Deseja desassociar este paciente ?"),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await meusPacientesEditController.desassociar();
                        meusPacientesEditController.botaoDesassociar = false;
                        await meuspacientesController.getMeusPacientesList();
                        Navigator.pop(context);
                        Modular.to.pushNamed('/meuspacientes');
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        meusPacientesEditController.botaoDesassociar = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });

    reaction((_) => meusPacientesEditController.botaoBaixa, (botaoBaixa) {
      if (botaoBaixa == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(Icons.warning),
              content: Text("Deseja dar baixa neste paciente ?"),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await meusPacientesEditController.baixa();
                        await meuspacientesController.getMeusPacientesList();
                        meusPacientesEditController.botaoBaixa = false;
                        Navigator.pop(context);
                        Modular.to.pop('/meuspacientes');
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        meusPacientesEditController.botaoBaixa = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });

    reaction((_) => meusPacientesEditController.botaoHL7, (botaoHL7) {
      if (botaoHL7 == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Deseja enviar os dados deste paciente ?"),
              contentPadding: const EdgeInsets.all(16.0),
              content: TextFormField(
                autofocus: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Email",
                ),
                onChanged: meusPacientesEditController.setEmail,
              ),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await meusPacientesEditController.hl7();
                        meusPacientesEditController.botaoHL7 = false;
                        Navigator.pop(context);
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        meusPacientesEditController.botaoHL7 = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });

    reaction((_) => meusPacientesEditController.verifyEmail, (email) {
      if (email == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(
                Icons.info,
                size: 40,
              ),
              content: Text(
                'Para sua segurança pedimos que o destinatario do email faça a verificação de autenticidade que chegara pelo endereço Amazon AWS ao email:  ${meusPacientesEditController.email}.'
                "Somente após a verificação o usuario estará disponivel para o cadastro no sistema."
                "Caso o usuario ja tenha feito a verificação, insira o email novamente",
                textAlign: TextAlign.justify,
              ),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("OK"),
                      onPressed: () {
                        meusPacientesEditController.verifyEmail = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
          backgroundColor: Colors.white,
        ),
        body: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Nome",
                              errorText:
                                  meusPacientesEditController.validarNome(
                                      meusPacientesEditController.nome),
                            ),
                            initialValue: meusPacientesEditController.nome,
                            onChanged: meusPacientesEditController.setNome,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //EMAIL
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Idade",
                              errorText:
                                  meusPacientesEditController.validarIdade(
                                      meusPacientesEditController.idade),
                            ),
                            initialValue: meusPacientesEditController.idade,
                            onChanged: meusPacientesEditController.setIdade,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //CRM
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Sexo",
                              errorText:
                                  meusPacientesEditController.validarSexo(
                                      meusPacientesEditController.sexo),
                            ),
                            initialValue: meusPacientesEditController.sexo,
                            onChanged: meusPacientesEditController.setSexo,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      //CRE
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Pressao",
                              errorText:
                                  meusPacientesEditController.validarPressao(
                                      meusPacientesEditController.pressao),
                            ),
                            initialValue: meusPacientesEditController.pressao,
                            onChanged: meusPacientesEditController.setPressao,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Temperatura",
                              errorText:
                                  meusPacientesEditController.validarTemp(
                                      meusPacientesEditController.temp),
                            ),
                            initialValue: meusPacientesEditController.temp,
                            onChanged: meusPacientesEditController.setTemp,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Risco",
                              errorText:
                                  meusPacientesEditController.validarRisco(
                                      meusPacientesEditController.risco),
                            ),
                            initialValue: meusPacientesEditController.risco,
                            onChanged: meusPacientesEditController.setRisco,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Local",
                              errorText:
                                  meusPacientesEditController.validarLocal(
                                      meusPacientesEditController.local),
                            ),
                            initialValue: meusPacientesEditController.local,
                            onChanged: meusPacientesEditController.setLocal,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Exames",
                                errorText:
                                    meusPacientesEditController.validarExames(
                                        meusPacientesEditController.exames)),
                            initialValue: meusPacientesEditController.exames,
                            onChanged: meusPacientesEditController.setExames,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 26,
                      ),
                      Observer(
                        builder: (_) {
                          return TextFormField(
                            autofocus: false,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Medicação",
                                errorText: meusPacientesEditController
                                    .validarMedicamentos(
                                        meusPacientesEditController
                                            .medicamentos)),
                            initialValue:
                                meusPacientesEditController.medicamentos,
                            onChanged:
                                meusPacientesEditController.setMedicamentos,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 80,
                  color: Color(0x62ffffff),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Observer(
                        builder: (_) {
                          return SizedBox(
                            height: 50,
                            width: 80,
                            child: RaisedButton(
                              color: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32),
                              ),
                              disabledColor:
                                  Theme.of(context).primaryColor.withAlpha(100),
                              textColor: Colors.white,
                              child: Text("Salvar"),
                              onPressed:
                                  meusPacientesEditController.savePressed,
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: 50,
                        width: 80,
                        child: RaisedButton(
                            color: Colors.red[200],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32),
                            ),
                            textColor: Colors.white,
                            child: Text("Desassociar"),
                            onPressed:
                                meusPacientesEditController.clicaBotaoDes),
                      ),
                      SizedBox(
                        height: 50,
                        width: 80,
                        child: RaisedButton(
                            color: Colors.blue,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32),
                            ),
                            textColor: Colors.white,
                            child: Text("Baixa"),
                            onPressed:
                                meusPacientesEditController.clicaBotaoBaixa),
                      ),
                      SizedBox(
                        height: 50,
                        width: 80,
                        child: RaisedButton(
                            color: Colors.purpleAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32),
                            ),
                            textColor: Colors.white,
                            child: Text("Enviar"),
                            onPressed:
                                meusPacientesEditController.clicaBotaoHL7),
                      ),
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }
}
