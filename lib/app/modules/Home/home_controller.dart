import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/models/paciente_model.dart';
import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:app_tcc/app/modules/shared/repositories/home_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final HomeRepository repository;
  final appController = Modular.get<AppController>();

  _HomeControllerBase(this.repository) {
    getList();
  }

  @observable
  ObservableStream<List<Paciente>> pacienteList;

  @observable
  String nome = "";

  String id = "";

  getNomeShered() async {
    await LocalStorage.getValue<String>('nome').then((value) {
      nome = value;
      print("nome shered homeController: " + nome);
    });
  }

  getCodigoShered() async {
    await LocalStorage.getValue<int>('id').then((value) {
      print(value);
      id = value.toString();
      print("codigo shered homeController: " + value.toString());
    });
  }

  removeValuesShered() async {
    await LocalStorage.removeValue('id');
    await LocalStorage.removeValue('nome');
    await LocalStorage.removeValue('nivel');
    print("Removido shered");
  }

  @action
  getList() {
    pacienteList = repository.list().asObservable();
  }

  @action
  adicionaMeusPacientes(String reference) async {
    await getCodigoShered();
    print(reference);
    await repository.update(reference, this.id);
    return;
  }
}
