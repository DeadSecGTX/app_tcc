import 'package:cloud_firestore/cloud_firestore.dart';

class Paciente {
  String key;
  String nome;
  String idade;
  String sexo;
  String pressao;
  String temperatura;
  String risco;
  String local;
  String exame;
  String medicacao;
  final DocumentReference reference;
  String user_id;

  Paciente(
      {this.reference,
      this.key,
      this.nome,
      this.idade,
      this.sexo,
      this.pressao,
      this.temperatura,
      this.risco,
      this.local,
      this.exame,
      this.medicacao,
      this.user_id});

  factory Paciente.fromDocument(DocumentSnapshot doc) {
    print(doc.id);
    print(doc.data()['user_id']);
    return Paciente(
        key: doc.id,
        nome: doc.data()['nome'],
        idade: doc.data()['idade'],
        sexo: doc.data()['sexo'],
        pressao: doc.data()['pressao'],
        temperatura: doc.data()['temp'],
        risco: doc.data()['risco'],
        local: doc.data()['local'],
        exame: doc.data()['exames'],
        medicacao: doc.data()['medicamentos'],
        reference: doc.reference,
        user_id: doc.data()['user_id']);
  }

  update(String id) {
    reference.update({'user_id': id});
  }

  updatePaciente(
    String nome,
    String idade,
    String sexo,
    String pressao,
    String temp,
    String risco,
    String local,
    String exame,
    String medic,
  ) {
    reference.update({
      'nome': nome,
      'idade': idade,
      'sexo': sexo,
      'pressao': pressao,
      'temp': temp,
      'risco': risco,
      'local': local,
      'exames': exame,
      'medicamentos': medic
    });
  }
}
