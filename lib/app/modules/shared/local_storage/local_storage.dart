import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static Future<dynamic> getValue<T>(String key) async {
    return await setIntance().then((sheredPreferences) {
      switch (T) {
        case double:
          return sheredPreferences.getDouble(key) ?? 0;
          break;
        case int:
          return sheredPreferences.getInt(key) ?? -1;
          break;
        case String:
          return sheredPreferences.getString(key) ?? '';
          break;
        case List:
          return sheredPreferences.getStringList(key) ?? [];
          break;
        case bool:
          return sheredPreferences.getBool(key) ?? false;
          break;
        default:
          return sheredPreferences.getString(key) ?? '';
      }
    });
  }

  static Future<SharedPreferences> setIntance() async {
    return await SharedPreferences.getInstance();
  }

  static Future<bool> setValue<T>(String key, dynamic value) async {
    return await setIntance().then((sheredPreferences) {
      switch (T) {
        case double:
          return sheredPreferences.setDouble(key, value);
          break;
        case int:
          return sheredPreferences.setInt(key, value);
          break;
        case String:
          return sheredPreferences.setString(key, value);
          break;
        case List:
          return sheredPreferences.setStringList(key, value);
          break;
        case bool:
          return sheredPreferences.setBool(key, value);
          break;
        default:
          return sheredPreferences.setString(key, value);
      }
    });
  }

  static Future<bool> removeValue(String key) async {
    return await setIntance().then((sheredPreferences) {
      return sheredPreferences.remove(key);
    });
  }
}
