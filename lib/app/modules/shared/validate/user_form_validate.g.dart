// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_form_validate.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserFormValidate on _UserFormValidate, Store {
  Computed<bool> _$isNomeValidateComputed;

  @override
  bool get isNomeValidate =>
      (_$isNomeValidateComputed ??= Computed<bool>(() => super.isNomeValidate,
              name: '_UserFormValidate.isNomeValidate'))
          .value;
  Computed<bool> _$isEmailValidateComputed;

  @override
  bool get isEmailValidate =>
      (_$isEmailValidateComputed ??= Computed<bool>(() => super.isEmailValidate,
              name: '_UserFormValidate.isEmailValidate'))
          .value;
  Computed<bool> _$isCrmValidateComputed;

  @override
  bool get isCrmValidate =>
      (_$isCrmValidateComputed ??= Computed<bool>(() => super.isCrmValidate,
              name: '_UserFormValidate.isCrmValidate'))
          .value;
  Computed<bool> _$isCreValidateComputed;

  @override
  bool get isCreValidate =>
      (_$isCreValidateComputed ??= Computed<bool>(() => super.isCreValidate,
              name: '_UserFormValidate.isCreValidate'))
          .value;
  Computed<bool> _$crmSelectComputed;

  @override
  bool get crmSelect =>
      (_$crmSelectComputed ??= Computed<bool>(() => super.crmSelect,
              name: '_UserFormValidate.crmSelect'))
          .value;
  Computed<bool> _$creSelectComputed;

  @override
  bool get creSelect =>
      (_$creSelectComputed ??= Computed<bool>(() => super.creSelect,
              name: '_UserFormValidate.creSelect'))
          .value;

  final _$idAtom = Atom(name: '_UserFormValidate.id');

  @override
  int get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  final _$nomeAtom = Atom(name: '_UserFormValidate.nome');

  @override
  String get nome {
    _$nomeAtom.reportRead();
    return super.nome;
  }

  @override
  set nome(String value) {
    _$nomeAtom.reportWrite(value, super.nome, () {
      super.nome = value;
    });
  }

  final _$emailAtom = Atom(name: '_UserFormValidate.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$crmAtom = Atom(name: '_UserFormValidate.crm');

  @override
  String get crm {
    _$crmAtom.reportRead();
    return super.crm;
  }

  @override
  set crm(String value) {
    _$crmAtom.reportWrite(value, super.crm, () {
      super.crm = value;
    });
  }

  final _$creAtom = Atom(name: '_UserFormValidate.cre');

  @override
  String get cre {
    _$creAtom.reportRead();
    return super.cre;
  }

  @override
  set cre(String value) {
    _$creAtom.reportWrite(value, super.cre, () {
      super.cre = value;
    });
  }

  final _$crmcheckAtom = Atom(name: '_UserFormValidate.crmcheck');

  @override
  bool get crmcheck {
    _$crmcheckAtom.reportRead();
    return super.crmcheck;
  }

  @override
  set crmcheck(bool value) {
    _$crmcheckAtom.reportWrite(value, super.crmcheck, () {
      super.crmcheck = value;
    });
  }

  final _$crecheckAtom = Atom(name: '_UserFormValidate.crecheck');

  @override
  bool get crecheck {
    _$crecheckAtom.reportRead();
    return super.crecheck;
  }

  @override
  set crecheck(bool value) {
    _$crecheckAtom.reportWrite(value, super.crecheck, () {
      super.crecheck = value;
    });
  }

  final _$_UserFormValidateActionController =
      ActionController(name: '_UserFormValidate');

  @override
  void setNome(String value) {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.setNome');
    try {
      return super.setNome(value);
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail(String value) {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.setEmail');
    try {
      return super.setEmail(value);
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCrm(String value) {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.setCrm');
    try {
      return super.setCrm(value);
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setCre(String value) {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.setCre');
    try {
      return super.setCre(value);
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic selecionaCrm() {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.selecionaCrm');
    try {
      return super.selecionaCrm();
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic selecionaCre() {
    final _$actionInfo = _$_UserFormValidateActionController.startAction(
        name: '_UserFormValidate.selecionaCre');
    try {
      return super.selecionaCre();
    } finally {
      _$_UserFormValidateActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
id: ${id},
nome: ${nome},
email: ${email},
crm: ${crm},
cre: ${cre},
crmcheck: ${crmcheck},
crecheck: ${crecheck},
isNomeValidate: ${isNomeValidate},
isEmailValidate: ${isEmailValidate},
isCrmValidate: ${isCrmValidate},
isCreValidate: ${isCreValidate},
crmSelect: ${crmSelect},
creSelect: ${creSelect}
    ''';
  }
}
