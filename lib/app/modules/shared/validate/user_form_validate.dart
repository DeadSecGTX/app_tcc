import 'package:mobx/mobx.dart';

part 'user_form_validate.g.dart';

class UserFormValidate = _UserFormValidate with _$UserFormValidate;

abstract class _UserFormValidate with Store {
  @observable
  int id = 0;

  @observable
  String nome = "";

  @action
  void setNome(String value) => nome = value;

  @observable
  String email = "";

  @action
  void setEmail(String value) => email = value;

  @observable
  String crm = "";

  @action
  void setCrm(String value) => crm = value;

  @observable
  String cre = "";

  @action
  void setCre(String value) => cre = value;

  @computed
  bool get isNomeValidate => nome != "" && nome.length >= 5;

  @computed
  bool get isEmailValidate => email != "" && nome.length >= 5;

  @computed
  bool get isCrmValidate => crm != "" && crm.length >= 5;

  @computed
  bool get isCreValidate => cre != "" && cre.length >= 5;

  @observable
  bool crmcheck = true;

  @observable
  bool crecheck = false;

  @action
  selecionaCrm() {
    this.crmcheck = true;
    this.crecheck = false;
    this.cre = "";
  }

  @action
  selecionaCre() {
    this.crecheck = true;
    this.crmcheck = false;
    this.crm = "";
  }

  @computed
  bool get crmSelect =>
      isNomeValidate && isEmailValidate && isCrmValidate && crmcheck;

  @computed
  bool get creSelect =>
      isNomeValidate && isEmailValidate && isCreValidate && crecheck;

  String validarNome(String nome) {
    if (nome == null || nome.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (nome.length < 5) {
      return "O nome precisa de no minimo de 5 caracteres";
    }

    return null;
  }

  String validarEmail(String email) {
    /* if (email == null || email.isEmpty) {
      return "Este campo é obrigatorio";*/
    if (!email.contains("@")) {
      return "Este email não é valido";
    }

    return null;
  }

  String validarCrm(String crm) {
    if (crm == null || crm.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (crm.length < 5) {
      return "O crm precisa de no minimo de 5 caracteres";
    }

    return null;
  }

  String validarCre(String cre) {
    if (cre == null || cre.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (cre.length < 5) {
      return "O cre precisa de no minimo de 5 caracteres";
    }

    return null;
  }
}
