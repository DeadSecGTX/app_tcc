import 'package:connectivity/connectivity.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../app_controller.dart';

class ConnectValidate {
  final appController = Modular.get<AppController>();

  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      appController.limparConnect();
      return true;
    } else
      appController.error_connect = true;
    return false;
  }
}
