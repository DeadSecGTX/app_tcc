import 'dart:convert';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:http/http.dart' as http;

class QrScanRepository {
  Future<Usuario> qrscan(String url) async {
    http.Response response = await http.get(url);
    var user = jsonDecode(response.body);

    print("$user");

    Usuario u = Usuario(
        id: user["id"],
        nome: user["nome"],
        email: user["email"],
        crm: user["crm"],
        cre: user["cre"],
        nivel: user["nivel_id"],
        status: user["status"]);

    return u;
  }
}
