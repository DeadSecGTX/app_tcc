import 'dart:async';
import 'dart:convert';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class AdmRepository {
  Future<List<Usuario>> listarUsuarios() async {
    http.Response response = await http.get(URL_BASE + "/index");
    var dadosJson = json.decode(response.body);
    List<Usuario> usuarios = [];
    for (var user in dadosJson) {
      print("usuario " + user["nome"]);
      Usuario u = Usuario(
          id: user["id"],
          nome: user["nome"],
          email: user["email"],
          crm: user["crm"],
          cre: user["cre"],
          nivel: user["nivel_id"],
          status: user["status"]);
      usuarios.add(u);
    }

    return usuarios;
  }

  Future<List<Usuario>> listarSearch(String search) async {
    print("Pesquisa: " + search);

    http.Response response = await http.get(
      URL_BASE + "/listar/$search",
      headers: {"Content-Type": "application/json"},
    );
    var dadosJson = json.decode(response.body);
    List<Usuario> usuarios = [];
    for (var user in dadosJson) {
      print("usuario " + user["nome"]);
      Usuario u = Usuario(
          id: user["id"],
          nome: user["nome"],
          email: user["email"],
          crm: user["crm"],
          cre: user["cre"],
          nivel: user["nivel_id"],
          status: user["status"]);
      usuarios.add(u);
    }

    return usuarios;
  }
}
