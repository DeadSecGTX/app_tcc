import 'dart:convert';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class LoginRepository{

  Future<Usuario> postLogin(String login, String senha) async{
    print(login + "" + senha);
    var corpo = json.encode({ "login": login, "senha": senha});

    print("Body: $corpo");

    http.Response response = await http.post(URL_BASE + "/logar",
        headers: { "Content-Type": "application/json" },
        body: corpo
    );

    var user = jsonDecode(response.body);

    print("$user");

    Usuario u = Usuario(id: user["id"], nome: user["nome"], email: user["email"], crm: user["crm"], cre: user["cre"], nivel: user["nivel_id"], status: user["status"]);

    return u;

  }
}