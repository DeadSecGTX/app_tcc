import 'dart:async';
import 'dart:convert';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class AdmNovoEmailRepository {
  Future<bool> listaVerify(String email) async {
    http.Response response = await http.get(
      URL_EMAIL + "/list",
      headers: {"Content-Type": "application/json"},
    );

    Map<String, dynamic> map = json.decode(response.body.toString());
    List<dynamic> data = [];
    data = map["VerifiedEmailAddresses"];

    //-1 entra pq é padrão do metodo pra falar se não achou
    print(data.toString() + " lista verify");
    if (data.indexWhere((element) => data.contains(email)) != -1) {
      return true;
    } else
      return false;
  }

  Future<bool> verify(String email) async {
    var corpo = json.encode({"email": email});

    print("Body: $corpo");

    http.Response response = await http.post(URL_EMAIL + "/verify",
        headers: {"Content-Type": "application/json"}, body: corpo);

    Map<String, dynamic> map = json.decode(response.body.toString());
    var data = map["ResponseMetadata"];

    print(data.toString());
    if (data != null) {
      print(data.toString());
      return true;
    } else {
      print(data.toString() + " erro");
      return false;
    }
  }
}
