import 'dart:convert';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class AdmEditRepository{
  Future<Usuario> AlterarUsuario(int id, String nome, String email, String crm, String cre) async{

    print(id.toString() + " " + nome + " " + email + " " + crm + " " + cre);
    var corpo = json.encode({ "nome": nome, "email": email, "crm": crm, "cre": cre});

    print("Body: $corpo");

    http.Response response = await http.post(URL_BASE + "/alterar/$id",
        headers: { "Content-Type": "application/json" },
        body: corpo
    );

    var user = jsonDecode(response.body);

    print("$user");

    Usuario u = Usuario(id: user["id"], nome: user["nome"], email: user["email"], crm: user["crm"], cre: user["cre"], nivel: user["nivel_id"], status: user["status"]);

    return u;

  }
}