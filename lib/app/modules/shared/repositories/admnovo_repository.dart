import 'dart:convert';
import 'package:app_tcc/app/modules/models/post_model.dart';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class AdmNovoRepository {
  Future<Usuario> NovoUsuario(String nome, String email, String crm, String cre,
      int nivel, bool status) async {
    print(" " +
        nome +
        " " +
        email +
        " " +
        crm +
        " " +
        cre +
        "" +
        nivel.toString() +
        "" +
        status.toString());
    var corpo = json.encode({
      "nome": nome,
      "email": email,
      "crm": crm,
      "cre": cre,
      "nivel": nivel,
      "status": status
    });

    print("Body: $corpo");

    http.Response response = await http.post(URL_BASE + "/users",
        headers: {"Content-Type": "application/json"}, body: corpo);

    var user = jsonDecode(response.body);

    print("$user");

    Usuario u = Usuario(
        id: user["id"],
        nome: user["nome"],
        email: user["email"],
        crm: user["crm"],
        cre: user["cre"],
        nivel: user["nivel_id"],
        status: user["status"]);

    return u;
  }

  Future<Post> salvarJson(
      int id, String nome, String email, String crm, String cre) async {
    print(
        id.toString() + " " + nome + " " + email + " " + crm + " " + cre + "");

    var corpo = json.encode(
        {"id": id, "nome": nome, "email": email, "crm": crm, "cre": cre});

    print("Body: $corpo");

    http.Response response = await http.post(URL_JSON + "/posts",
        headers: {"Content-Type": "application/json"}, body: corpo);

    var post = jsonDecode(response.body);

    Post p = Post(
        id: post["_id"],
        email: post["email"],
        key: post["key"],
        url: post["url"]);

    return p;
  }

  Future enviarEmail(String email, String url) async {
    print(" " + email + " " + url + " ");

    var corpo = json.encode({"email": email, "url": url});

    print("Body: $corpo");

    http.Response response = await http.post(URL_EMAIL + "/send",
        headers: {"Content-Type": "application/json"}, body: corpo);

    var msg = response.body;

    return msg;
  }

  Future excluirProvisorio(int id) async {
    print(" " + id.toString());

    http.Response response = await http.post(
      URL_BASE + "/excluir/$id",
      headers: {"Content-Type": "application/json"},
    );

    var msg = jsonDecode(response.body);

    return msg;
  }

  Future excluirMongoS3(String id, String key) async {
    print(" " + id + "" + key + "");

    var corpo = json.encode({"id": id, "key": key});

    print("Body: $corpo");

    http.Response response = await http.post(URL_JSON + "/deletar",
        headers: {"Content-Type": "application/json"}, body: corpo);

    var msg = jsonDecode(response.body);

    return msg;
  }
}
