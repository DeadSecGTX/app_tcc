import 'dart:convert';
import 'package:app_tcc/app/modules/models/login_model.dart';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:http/http.dart' as http;

class NewLoginRepository {
  Future<Login> novoLogin(int id, String login, String senha) async {
    print(id.toString() + " " + login + " " + senha);

    var corpo = json.encode({"id": id, "login": login, "senha": senha});

    print("Body: $corpo");

    http.Response response = await http.post(URL_BASE + '/users/$id/login',
        headers: {"Content-Type": "application/json"}, body: corpo);

    var post = jsonDecode(response.body);

    Login l = Login(id: post["id"], login: post["login"], senha: post["senha"]);

    return l;
  }

  Future<Usuario> ativar(int id) async {
    print(id.toString());
    http.Response response = await http.post(URL_BASE + '/logar/$id/ativar',
        headers: {"Content-Type": "application/json"});

    var user = jsonDecode(response.body);

    Usuario u = Usuario(
        id: user["id"],
        nome: user["nome"],
        email: user["email"],
        crm: user["crm"],
        cre: user["cre"],
        nivel: user["nivel_id"],
        status: user["status"]);

    return u;
  }
}
