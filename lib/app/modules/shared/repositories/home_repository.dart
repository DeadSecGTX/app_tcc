import 'dart:async';
import 'dart:convert';
import 'package:app_tcc/app/modules/models/paciente_model.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;

class HomeRepository {
  final FirebaseFirestore firestore;

  HomeRepository(this.firestore);

  Stream<List<Paciente>> list() {
    return firestore.collection('pacientes').snapshots().map((query) {
      return query.docs.map((doc) {
        return Paciente.fromDocument(doc);
      }).toList();
    });
  }

  Stream<List<Paciente>> listMeusPacientes(String id) {
    print("id shered " + id);
    return firestore
        .collection('pacientes')
        .where('user_id', isEqualTo: id)
        .snapshots()
        .map((query) {
      return query.docs.map((doc) {
        return Paciente.fromDocument(doc);
      }).toList();
    });
  }

  Future<void> update(String reference, String id) async {
    await firestore
        .collection('pacientes')
        .doc(reference)
        .update({'user_id': id});

    return;
  }

  Future<void> updatePacientes(
    String reference,
    String nome,
    String idade,
    String sexo,
    String pressao,
    String temp,
    String risco,
    String local,
    String exame,
    String medic,
  ) async {
    await firestore.collection('pacientes').doc(reference).update({
      'nome': nome,
      'idade': idade,
      'sexo': sexo,
      'pressao': pressao,
      'temp': temp,
      'risco': risco,
      'local': local,
      'exames': exame,
      'medicamentos': medic
    });

    return;
  }

  Future<void> baixa(String reference) async {
    await firestore
        .collection('pacientes')
        .doc(reference)
        .update({'user_id': "-1"});

    return;
  }

  Future<bool> listaVerify(String email) async {
    http.Response response = await http.get(
      URL_EMAIL + "/list",
      headers: {"Content-Type": "application/json"},
    );

    Map<String, dynamic> map = json.decode(response.body.toString());

    List<dynamic> data = [];

    data = map["VerifiedEmailAddresses"];

    //-1 entra pq é padrão do metodo pra falar se não achou
    print(data.toString() + " lista verify");
    if (data.indexWhere((element) => data.contains(email)) != -1) {
      return true;
    } else
      return false;
  }

  Future<bool> verify(String email) async {
    var corpo = json.encode({"email": email});

    print("Body: $corpo");

    http.Response response = await http.post(URL_EMAIL + "/verify",
        headers: {"Content-Type": "application/json"}, body: corpo);

    Map<String, dynamic> map = json.decode(response.body.toString());
    var data = map["ResponseMetadata"];

    print(data.toString());
    if (data != null) {
      print(data.toString());
      return true;
    } else {
      print(data.toString() + " erro");
      return false;
    }
  }

  Future<void> hl7(String reference, String email) async {
    var corpo = json.encode({"id": reference, "email": email});
    print("Body: $corpo");

    http.Response response = await http.post(URL_FIREBASE + "/listar",
        headers: {"Content-Type": "application/json"}, body: corpo);

    var msg = response.body;

    return msg;
  }
}
