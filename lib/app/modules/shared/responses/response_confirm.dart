import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class ResponseConfirm extends StatefulWidget {
  final IconData icon;
  final String mensagem;
  final Future<void> functYes;

  const ResponseConfirm({Key key, this.icon, this.mensagem, this.functYes})
      : super(key: key);

  @override
  _ResponseConfirmState createState() => _ResponseConfirmState();
}

class _ResponseConfirmState extends State<ResponseConfirm> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Icon(
        widget.icon,
        color: Colors.yellow,
        size: 40,
      ),
      content: Text(
        widget.mensagem,
        textAlign: TextAlign.justify,
        style: TextStyle(color: Colors.black),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              color: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ),
              textColor: Colors.white,
              child: Text("Sim"),
              onPressed: () async {
                await widget.functYes;
                Modular.to.pop();
              },
            ),
            RaisedButton(
              color: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ),
              textColor: Colors.white,
              child: Text("Não"),
              onPressed: () async {
                Modular.to.pop();
              },
            ),
          ],
        )
      ],
    );
  }
}
