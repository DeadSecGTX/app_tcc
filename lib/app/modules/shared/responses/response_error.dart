import 'package:app_tcc/app/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../../app_controller.dart';

class ResponseError extends StatefulWidget {
  final IconData icon;
  final String mensagem;
  final Future<void> funct;

  const ResponseError({Key key, this.icon, this.mensagem, this.funct})
      : super(key: key);

  @override
  _ResponseErrorState createState() => _ResponseErrorState();
}

class _ResponseErrorState extends State<ResponseError> {
  final appController = Modular.get<AppController>();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Icon(
        widget.icon,
        color: Colors.yellow,
        size: 40,
      ),
      content: Text(
        widget.mensagem,
        textAlign: TextAlign.justify,
        style: TextStyle(color: Colors.black),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RaisedButton(
              color: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32),
              ),
              textColor: Colors.white,
              child: Text("Ok"),
              onPressed: () async {
                await widget.funct;
                Navigator.pop(context);
              },
            )
          ],
        )
      ],
    );
  }
}
