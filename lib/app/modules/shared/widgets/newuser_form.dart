import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/shared/widgets/select_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class NewUserForm extends StatefulWidget {
  @override
  _NewUserFormState createState() => _NewUserFormState();
}

class _NewUserFormState extends State<NewUserForm> {
  @override
  Widget build(BuildContext context) {
    final userFormValidate = Modular.get<UserFormValidate>();

    return SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
                //mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  //Nome
                  Observer(
                    builder: (_) {
                      return TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Nome",
                          errorText: userFormValidate
                              .validarNome(userFormValidate.nome),
                        ),
                        initialValue: userFormValidate.nome,
                        onChanged: userFormValidate.setNome,
                      );
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  //EMAIL
                  Observer(
                    builder: (_) {
                      return TextFormField(
                        autofocus: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Email",
                          errorText: userFormValidate
                              .validarEmail(userFormValidate.email),
                        ),
                        initialValue: userFormValidate.email,
                        onChanged: userFormValidate.setEmail,
                      );
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  SelectRow(),
                ])));
  }
}
