import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SelectRow extends StatefulWidget {
  @override
  _SelectRowState createState() => _SelectRowState();
}

class _SelectRowState extends State<SelectRow> {
  final userFormValidate = Modular.get<UserFormValidate>();

  TextEditingController _controllerCRM = new TextEditingController();
  TextEditingController _controllerCRE = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _controllerCRM = new TextEditingController(text: userFormValidate.crm);
    _controllerCRE = new TextEditingController(text: userFormValidate.cre);
  }

  clearCRM() {
    _controllerCRM.clear();
  }

  clearCRE() {
    _controllerCRE.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 700,
        height: 200,
        child: Column(children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Observer(
                  builder: (_) {
                    return Container(
                      width: 120.0,
                      height: 80.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          width: 6,
                          color: userFormValidate.crmcheck
                              ? Colors.blue
                              : Colors.black,
                        ),
                      ),
                      child: GestureDetector(
                        onTap: () {
                          userFormValidate.selecionaCrm();
                          clearCRE();
                          print("crm select: " +
                              userFormValidate.crmSelect.toString());
                        },
                        child: Image(
                          image: AssetImage("imagens/medico.png"),
                          fit: BoxFit.cover,
                          height: 20,
                        ),
                      ),
                    );
                  },
                ),
                Observer(
                  builder: (_) {
                    return Container(
                      width: 120.0,
                      height: 80.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                            width: 6,
                            color: userFormValidate.crecheck
                                ? Colors.blue
                                : Colors.black),
                      ),
                      child: GestureDetector(
                        onTap: () async {
                          await userFormValidate.selecionaCre();
                          clearCRM();
                          print("cre select: " +
                              userFormValidate.creSelect.toString());
                        },
                        child: Image(
                          image: AssetImage("imagens/enfermagem.png"),
                          fit: BoxFit.cover,
                          height: 20,
                        ),
                      ),
                    );
                  },
                ),
              ]),
          const SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              SizedBox(
                width: 140.0,
                height: 100.0,
                child: Observer(
                  builder: (_) {
                    return TextFormField(
                      enabled: userFormValidate.crmcheck,
                      autofocus: false,
                      controller: _controllerCRM,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "CRM",
                        errorText:
                            userFormValidate.validarCrm(userFormValidate.crm),
                      ),
                      //initialValue: userFormValidate.crm,
                      onChanged: userFormValidate.setCrm,
                    );
                  },
                ),
              ),
              Observer(builder: (_) {
                return Container(
                  width: 140.0,
                  height: 100.0,
                  child: TextFormField(
                    enabled: userFormValidate.crecheck,
                    autofocus: false,
                    controller: _controllerCRE,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "CRE",
                      errorText:
                          userFormValidate.validarCre(userFormValidate.cre),
                    ),
                    //initialValue: userFormValidate.cre,
                    onChanged: userFormValidate.setCre,
                  ),
                );
              }),
              const SizedBox(
                height: 16,
              )
            ],
          )
        ]));
  }
}
