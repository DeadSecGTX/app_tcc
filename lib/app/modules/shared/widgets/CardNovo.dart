import 'package:app_tcc/app/modules/adm/admedit_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CardNovoWidget extends StatelessWidget {

  CardNovoWidget({this.nome, this.email, this.crm, this.cre, this.id, this.nivel, this.status});

  final int id;
  final String nome;
  final String email;
  final String crm;
  final String cre;
  final int nivel;
  final bool status;

  final admeditController = Modular.get<AdmEditController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 10),
            width: 40,
            height: 40,
            child: Image.asset(crm == null ? "imagens/enfermagem.png": "imagens/medico.jpg"),
          ),
          Expanded(
            flex: 2,
            child: Text(nome,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(crm == null ? cre : crm,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                ),
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.edit),
            iconSize: 16,
            onPressed: (){
              admeditController.passartela(id, nome, email, crm, cre);
            },
          ),
        ],
      ),
    );
  }
}