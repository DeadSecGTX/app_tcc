import 'package:flutter/material.dart';

class PacientesNovoWidget extends StatelessWidget {
  final String nome;
  final String risco;
  final bool status;

  PacientesNovoWidget({this.nome, this.status, this.risco});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 3,
              child: Center(
                child: Text(
                  nome,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              )),
        ],
      ),
    );
  }
}
