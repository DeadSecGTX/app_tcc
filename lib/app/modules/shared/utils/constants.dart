const String URL_BASE = 'http://52.86.207.245:3333';
const String URL_JSON = 'http://52.86.207.245:3000';
const String URL_EMAIL = 'http://52.86.207.245:8080';
const String URL_FIREBASE = 'http://52.86.207.245:4000';

const String splash = '/';
const String login = '/login';
const String primeiroLogin = '/primeirologin';
const String adm = '/adm';
const String admEditar = '/edit';
const String admNovo = '/novo';
