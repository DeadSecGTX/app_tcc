import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/login/login_novousuario_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/shared/widgets/custom_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class NovoUsuarioLogin extends StatefulWidget {
  @override
  _NovoUsuarioLoginState createState() => _NovoUsuarioLoginState();
}

class _NovoUsuarioLoginState extends State<NovoUsuarioLogin> {
  final appController = Modular.get<AppController>();
  final userFormValidate = Modular.get<UserFormValidate>();
  final loginNovoUsuarioController = Modular.get<LoginNovoUsuarioController>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
                title: Text("Deseja retornar a tela principal ?"),
                actions: <Widget>[
                  Observer(
                    builder: (_) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          child: Text(
                            "Sim",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          color: Colors.green,
                          disabledColor:
                              Theme.of(context).primaryColor.withAlpha(100),
                          onPressed: () async {
                            await loginNovoUsuarioController.inativarUsuario();
                            Modular.to.pushNamed(login);
                          });
                    },
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32),
                    ),
                    child: Text(
                      "Não",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ));
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: SafeArea(
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            //resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.grey, opacity: 0.3),
              backgroundColor: Colors.white,
              title: Text(
                "Escolha seu Login e Senha ",
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Observer(
                      builder: (_) {
                        return TextFormField(
                          autofocus: false,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(32)),
                              labelText: "Login",
                              errorText:
                                  loginNovoUsuarioController.validarLogin(
                                      loginNovoUsuarioController.login)),
                          onChanged: loginNovoUsuarioController.setLogin,
                        );
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    //EMAIL
                    Observer(
                      builder: (_) {
                        return TextFormField(
                          autofocus: false,
                          obscureText:
                              !loginNovoUsuarioController.passwordVisible,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32)),
                            labelText: "Senha",
                            errorText: loginNovoUsuarioController.validarSenha(
                                loginNovoUsuarioController.password),
                            suffix: CustomIconButton(
                              radius: 32,
                              iconData:
                                  loginNovoUsuarioController.passwordVisible
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                              onTap: loginNovoUsuarioController.senhaVisivel,
                            ),
                          ),
                          onChanged: loginNovoUsuarioController.setPassword,
                        );
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Observer(
                      builder: (_) {
                        return TextFormField(
                          autofocus: false,
                          obscureText:
                              !loginNovoUsuarioController.passwordVisible,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32)),
                            labelText: "Confirmar senha",
                            errorText:
                                loginNovoUsuarioController.confirmarSenha(
                                    loginNovoUsuarioController.confirmPassword),
                            suffix: CustomIconButton(
                              radius: 32,
                              iconData:
                                  loginNovoUsuarioController.passwordVisible
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                              onTap: loginNovoUsuarioController.senhaVisivel,
                            ),
                          ),
                          onChanged:
                              loginNovoUsuarioController.setConfirmPassword,
                        );
                      },
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: 130,
                          height: 50,
                          child: Observer(
                            builder: (_) {
                              return RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(32),
                                ),
                                child: loginNovoUsuarioController.loading
                                    ? CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            Colors.white),
                                      )
                                    : Text(
                                        "Proximo",
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                disabledColor: Theme.of(context)
                                    .primaryColor
                                    .withAlpha(100),
                                onPressed:
                                    loginNovoUsuarioController.salvarPressed,
                                color: Colors.green,
                              );
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
