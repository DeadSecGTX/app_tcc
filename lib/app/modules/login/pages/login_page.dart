import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/login/login_novousuario_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/widgets/custom_icon_button.dart';
import 'package:app_tcc/app/modules/shared/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../login_controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final loginController = Modular.get<LoginController>();
  final appController = Modular.get<AppController>();
  final loginNovoUsuarioController = Modular.get<LoginNovoUsuarioController>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.grey, opacity: 0.3),
          backgroundColor: Colors.white,
          title: Text("Login"),
          actions: <Widget>[
            //definir objetos de ação
            IconButton(
              icon: Icon(
                Icons.camera_alt,
                size: 30,
              ),
              onPressed: loginNovoUsuarioController.qrScanner,
            )
          ],
        ),
        body: Column(
          children: [
            SafeArea(
              child: Image(
                image: AssetImage("imagens/logo2.png"),
                fit: BoxFit.cover,
                height: 160,
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.all(32),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                elevation: 16,
                color: Colors.lightBlueAccent,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Observer(
                        builder: (_) {
                          return CustomTextField(
                            hint: 'Login',
                            prefix: Icon(Icons.account_circle),
                            textInputType: TextInputType.text,
                            onChanged: loginController.setLogin,
                            enabled: !loginController.loading,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Observer(
                        builder: (_) {
                          return CustomTextField(
                            hint: 'Senha',
                            prefix: Icon(Icons.lock),
                            obscure: !loginController.passwordVisible,
                            onChanged: loginController.setPassword,
                            enabled: !loginController.loading,
                            suffix: CustomIconButton(
                              radius: 32,
                              iconData: loginController.passwordVisible
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                              onTap: loginController.senhaVisivel,
                            ),
                            //controller: _controllerSenha,
                          );
                        },
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Observer(
                        builder: (_) {
                          return SizedBox(
                            height: 44,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(32),
                                ),
                                child: loginController.loading
                                    ? CircularProgressIndicator(
                                  valueColor:
                                  AlwaysStoppedAnimation(Colors.white),
                                )
                                    : Text(loginController.titulo),
                                color: Theme.of(context).primaryColor,
                                disabledColor:
                                Theme.of(context).primaryColor.withAlpha(100),
                                textColor: Colors.white,
                                onPressed: loginController.loginPressed),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        )
      ),
    );
  }
}
