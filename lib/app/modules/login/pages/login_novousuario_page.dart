import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/login/login_controller.dart';
import 'package:app_tcc/app/modules/login/login_novousuario_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/shared/widgets/newuser_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class LoginNovoUsuarioPage extends StatefulWidget {
  @override
  _LoginNovoUsuarioPageState createState() => _LoginNovoUsuarioPageState();
}

class _LoginNovoUsuarioPageState extends State<LoginNovoUsuarioPage> {
  final loginController = Modular.get<LoginController>();
  final loginNovoUsuarioController = Modular.get<LoginNovoUsuarioController>();
  final userFormValidate = Modular.get<UserFormValidate>();
  final appController = Modular.get<AppController>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // TODO: implement didChangeDependencies

    //Erros diversos nos web services
    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });

    //Erro de conexão
    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
                title: Text("Deseja retornar a tela principal ?"),
                actions: <Widget>[
                  Observer(
                    builder: (_) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          child: Text(
                            "Sim",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          color: Colors.green,
                          disabledColor:
                              Theme.of(context).primaryColor.withAlpha(100),
                          onPressed: () {
                            userFormValidate.id = 0;
                            userFormValidate.nome = "";
                            userFormValidate.email = "";
                            userFormValidate.crm = "";
                            userFormValidate.cre = "";
                            Modular.to.pushNamed(login);
                          });
                    },
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32),
                    ),
                    child: Text(
                      "Não",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ));
    }

    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
              backgroundColor: Colors.white,
              title: Text(
                " Confirme seus dados ",
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
            body: Column(
              children: [
                NewUserForm(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 130,
                      height: 50,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: loginNovoUsuarioController.loading
                            ? CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.white),
                              )
                            : Text(
                                "Proximo",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                        disabledColor:
                            Theme.of(context).primaryColor.withAlpha(100),
                        onPressed: () {
                          loginNovoUsuarioController.proximo();
                        },
                        color: Colors.green,
                      ),
                    ),
                    SizedBox(
                      width: 130,
                      height: 50,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: Text(
                          "Voltar",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {
                          userFormValidate.id = 0;
                          userFormValidate.nome = "";
                          userFormValidate.email = "";
                          userFormValidate.crm = "";
                          userFormValidate.cre = "";
                          Modular.to.pushNamed(login);
                        },
                        color: Colors.blue,
                      ),
                    ),
                  ],
                )
              ],
            )));
  }
}
