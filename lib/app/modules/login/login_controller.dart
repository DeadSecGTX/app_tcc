import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:app_tcc/app/modules/shared/repositories/qrscan_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/repositories/login_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../app_controller.dart';
part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  final loginRepository = Modular.get<LoginRepository>();
  final appController = Modular.get<AppController>();
  final qrRepository = Modular.get<QrScanRepository>();
  final userFormValidate = Modular.get<UserFormValidate>();

  limparQR() {}

  limparControllers() {
    password = "";
    login = "";
    loading = false;
  }

  addUserShered() async {
    await LocalStorage.setValue<int>('id', usuario.id);
    print("meu id shered é: " + usuario.id.toString());
    await LocalStorage.setValue<String>('nome', usuario.nome);
    print("meu nome shered é: " + usuario.nome.toString());
    await LocalStorage.setValue<int>('nivel', usuario.nivel);
    print("meu nivel shered é: " + usuario.nivel.toString());
  }

  @observable
  Usuario usuario;

  @observable
  bool loading = false;

  @observable
  String login = "";

  @action
  void setLogin(String value) => login = value;

  @observable
  String password = "";

  @action
  void setPassword(String value) => password = value;

  @observable
  bool passwordVisible = false;

  @observable
  String titulo = "Login";

  @action
  void senhaVisivel() => passwordVisible = !passwordVisible;

  @computed
  bool get isPasswordValid => password.length >= 1;

  @computed
  bool get isLoginValid => login.length >= 5;

  @computed
  bool get isFormValid => login.length > 5 && password.length > 1;

  @computed
  Function get loginPressed =>
      (isLoginValid && isPasswordValid && !loading) ? logar : null;

  @action
  logar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    if (teste == true) {
      loading = true;

      try {
        await loginRepository.postLogin(login, password).then((value) async {
          if (value.nome != null) {
            usuario = value;

            print("Logado com: " + usuario.nome);

            await addUserShered();

            titulo = "Login";

            if (usuario.nivel == 1) {
              Modular.to.pushNamed('/home/');
            } else {
              Modular.to.pushNamed('/adm/');
            }

            appController.limparGeneral(); //limpar observables de erros

            limparControllers(); //limpar campos do login

            return;
          } else {
            loading = false;
            //titulo = "Usuario ou senha incorretas";
            appController.error_general = true;
            appController.msg = "Usuario ou senha incorretas";
            return;
          }
        });
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao fazer o login";
        limparControllers();
      }
    } else {
      appController.error_connect = true;
      limparControllers();
    }
  }
}
