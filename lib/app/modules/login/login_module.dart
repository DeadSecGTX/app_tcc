import 'package:app_tcc/app/modules/Home/home_module.dart';
import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/adm/adm_module.dart';
import 'package:app_tcc/app/modules/login/login_novousuario_controller.dart';
import 'package:app_tcc/app/modules/login/pages/login_novousuario_page.dart';
import 'package:app_tcc/app/modules/login/pages/login_page.dart';
import 'package:app_tcc/app/modules/shared/repositories/adm_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/admedit_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/adminativar_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/login_newlogin_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/login_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/qrscan_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/splash/splash_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'login_controller.dart';

class LoginModule extends ChildModule {
  @override
  // TODO: implement binds
  List<Bind> get binds => [
        Bind((i) => LoginController()),
        Bind((i) => AdmController(i.get())),
        Bind((i) => AdmRepository()),
        Bind((i) => LoginRepository()),
        Bind((i) => ConnectValidate()),
        Bind((i) => LoginNovoUsuarioController()),
        Bind((i) => NewLoginRepository()),
        Bind((i) => AdmEditRepository()),
        Bind((i) => SplashController()),
        Bind((i) => QrScanRepository()),
        Bind((i) => UserFormValidate()),
        Bind((i) => AdmInativarRepository()),
      ];

  @override
  // TODO: implement routers
  List<Router> get routers => [
        Router('/', child: (_, args) => LoginPage()),
        Router('/novo', child: (_, args) => LoginNovoUsuarioPage()),
        Router('/adm', module: AdmModule()),
        Router('/home', module: HomeModule()),
      ];

  static Inject get to => Inject<LoginModule>.of();
}
