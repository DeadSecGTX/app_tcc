// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_novousuario_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoginNovoUsuarioController on _LoginNovoUsuarioControllerBase, Store {
  Computed<bool> _$isPasswordConfirmComputed;

  @override
  bool get isPasswordConfirm => (_$isPasswordConfirmComputed ??= Computed<bool>(
          () => super.isPasswordConfirm,
          name: '_LoginNovoUsuarioControllerBase.isPasswordConfirm'))
      .value;
  Computed<bool> _$isPasswordValidComputed;

  @override
  bool get isPasswordValid =>
      (_$isPasswordValidComputed ??= Computed<bool>(() => super.isPasswordValid,
              name: '_LoginNovoUsuarioControllerBase.isPasswordValid'))
          .value;
  Computed<bool> _$isLoginValidComputed;

  @override
  bool get isLoginValid =>
      (_$isLoginValidComputed ??= Computed<bool>(() => super.isLoginValid,
              name: '_LoginNovoUsuarioControllerBase.isLoginValid'))
          .value;
  Computed<bool> _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_LoginNovoUsuarioControllerBase.isFormValid'))
          .value;
  Computed<Function> _$salvarPressedComputed;

  @override
  Function get salvarPressed =>
      (_$salvarPressedComputed ??= Computed<Function>(() => super.salvarPressed,
              name: '_LoginNovoUsuarioControllerBase.salvarPressed'))
          .value;
  Computed<bool> _$isNomeChangeComputed;

  @override
  bool get isNomeChange =>
      (_$isNomeChangeComputed ??= Computed<bool>(() => super.isNomeChange,
              name: '_LoginNovoUsuarioControllerBase.isNomeChange'))
          .value;
  Computed<bool> _$isEmailChangeComputed;

  @override
  bool get isEmailChange =>
      (_$isEmailChangeComputed ??= Computed<bool>(() => super.isEmailChange,
              name: '_LoginNovoUsuarioControllerBase.isEmailChange'))
          .value;
  Computed<bool> _$isCrmChangeComputed;

  @override
  bool get isCrmChange =>
      (_$isCrmChangeComputed ??= Computed<bool>(() => super.isCrmChange,
              name: '_LoginNovoUsuarioControllerBase.isCrmChange'))
          .value;
  Computed<bool> _$isCreChangeComputed;

  @override
  bool get isCreChange =>
      (_$isCreChangeComputed ??= Computed<bool>(() => super.isCreChange,
              name: '_LoginNovoUsuarioControllerBase.isCreChange'))
          .value;
  Computed<bool> _$changeComputed;

  @override
  bool get change => (_$changeComputed ??= Computed<bool>(() => super.change,
          name: '_LoginNovoUsuarioControllerBase.change'))
      .value;

  final _$usuarioAtom = Atom(name: '_LoginNovoUsuarioControllerBase.usuario');

  @override
  Usuario get usuario {
    _$usuarioAtom.reportRead();
    return super.usuario;
  }

  @override
  set usuario(Usuario value) {
    _$usuarioAtom.reportWrite(value, super.usuario, () {
      super.usuario = value;
    });
  }

  final _$loadingAtom = Atom(name: '_LoginNovoUsuarioControllerBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$loginAtom = Atom(name: '_LoginNovoUsuarioControllerBase.login');

  @override
  String get login {
    _$loginAtom.reportRead();
    return super.login;
  }

  @override
  set login(String value) {
    _$loginAtom.reportWrite(value, super.login, () {
      super.login = value;
    });
  }

  final _$passwordAtom = Atom(name: '_LoginNovoUsuarioControllerBase.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$confirmPasswordAtom =
      Atom(name: '_LoginNovoUsuarioControllerBase.confirmPassword');

  @override
  String get confirmPassword {
    _$confirmPasswordAtom.reportRead();
    return super.confirmPassword;
  }

  @override
  set confirmPassword(String value) {
    _$confirmPasswordAtom.reportWrite(value, super.confirmPassword, () {
      super.confirmPassword = value;
    });
  }

  final _$passwordVisibleAtom =
      Atom(name: '_LoginNovoUsuarioControllerBase.passwordVisible');

  @override
  bool get passwordVisible {
    _$passwordVisibleAtom.reportRead();
    return super.passwordVisible;
  }

  @override
  set passwordVisible(bool value) {
    _$passwordVisibleAtom.reportWrite(value, super.passwordVisible, () {
      super.passwordVisible = value;
    });
  }

  final _$proximoAsyncAction =
      AsyncAction('_LoginNovoUsuarioControllerBase.proximo');

  @override
  Future<void> proximo() {
    return _$proximoAsyncAction.run(() => super.proximo());
  }

  final _$salvarLoginAsyncAction =
      AsyncAction('_LoginNovoUsuarioControllerBase.salvarLogin');

  @override
  Future<void> salvarLogin() {
    return _$salvarLoginAsyncAction.run(() => super.salvarLogin());
  }

  final _$inativarUsuarioAsyncAction =
      AsyncAction('_LoginNovoUsuarioControllerBase.inativarUsuario');

  @override
  Future<void> inativarUsuario() {
    return _$inativarUsuarioAsyncAction.run(() => super.inativarUsuario());
  }

  final _$qrScannerAsyncAction =
      AsyncAction('_LoginNovoUsuarioControllerBase.qrScanner');

  @override
  Future<void> qrScanner() {
    return _$qrScannerAsyncAction.run(() => super.qrScanner());
  }

  final _$_LoginNovoUsuarioControllerBaseActionController =
      ActionController(name: '_LoginNovoUsuarioControllerBase');

  @override
  void setLogin(String value) {
    final _$actionInfo = _$_LoginNovoUsuarioControllerBaseActionController
        .startAction(name: '_LoginNovoUsuarioControllerBase.setLogin');
    try {
      return super.setLogin(value);
    } finally {
      _$_LoginNovoUsuarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(String value) {
    final _$actionInfo = _$_LoginNovoUsuarioControllerBaseActionController
        .startAction(name: '_LoginNovoUsuarioControllerBase.setPassword');
    try {
      return super.setPassword(value);
    } finally {
      _$_LoginNovoUsuarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setConfirmPassword(String value) {
    final _$actionInfo =
        _$_LoginNovoUsuarioControllerBaseActionController.startAction(
            name: '_LoginNovoUsuarioControllerBase.setConfirmPassword');
    try {
      return super.setConfirmPassword(value);
    } finally {
      _$_LoginNovoUsuarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void senhaVisivel() {
    final _$actionInfo = _$_LoginNovoUsuarioControllerBaseActionController
        .startAction(name: '_LoginNovoUsuarioControllerBase.senhaVisivel');
    try {
      return super.senhaVisivel();
    } finally {
      _$_LoginNovoUsuarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
usuario: ${usuario},
loading: ${loading},
login: ${login},
password: ${password},
confirmPassword: ${confirmPassword},
passwordVisible: ${passwordVisible},
isPasswordConfirm: ${isPasswordConfirm},
isPasswordValid: ${isPasswordValid},
isLoginValid: ${isLoginValid},
isFormValid: ${isFormValid},
salvarPressed: ${salvarPressed},
isNomeChange: ${isNomeChange},
isEmailChange: ${isEmailChange},
isCrmChange: ${isCrmChange},
isCreChange: ${isCreChange},
change: ${change}
    ''';
  }
}
