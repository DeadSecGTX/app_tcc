import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/repositories/admedit_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/adminativar_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/login_newlogin_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/qrscan_repository.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../../app_controller.dart';

part 'login_novousuario_controller.g.dart';

class LoginNovoUsuarioController = _LoginNovoUsuarioControllerBase
    with _$LoginNovoUsuarioController;

abstract class _LoginNovoUsuarioControllerBase with Store {
  final appController = Modular.get<AppController>(); //usado pra erros
  final qrRepository =
      Modular.get<QrScanRepository>(); //usado para scann de qrcode
  final userFormValidate =
      Modular.get<UserFormValidate>(); //controllers globais
  final admEditRepository = Modular.get<AdmEditRepository>(); //metodo alterar
  final newLoginRepository =
      Modular.get<NewLoginRepository>(); //ativar registro
  final admInativarRepository =
      Modular.get<AdmInativarRepository>(); //inativar registro

  String nomeO, emailO, crmO, creO;

  limparOriginais() {
    nomeO = "";
    emailO = "";
    crmO = "";
    creO = "";
    loading = false;
  }

  limparObservables() {
    login = "";
    password = "";
    confirmPassword = "";
  }

  limparController() {
    userFormValidate.id = 0;
    userFormValidate.nome = "";
    userFormValidate.email = "";
    userFormValidate.crm = "";
    userFormValidate.cre = "";
  }

  @observable
  Usuario usuario;

  @observable
  bool loading = false;

  @observable
  String login = "";

  @action
  void setLogin(String value) => login = value;

  @observable
  String password = "";

  @action
  void setPassword(String value) => password = value;

  @observable
  String confirmPassword = "";

  @action
  void setConfirmPassword(String value) => confirmPassword = value;

  @observable
  bool passwordVisible = false;

  @action
  void senhaVisivel() => passwordVisible = !passwordVisible;

  @computed
  bool get isPasswordConfirm => password == confirmPassword;

  @computed
  bool get isPasswordValid => password.length >= 1;

  @computed
  bool get isLoginValid => login.length >= 5;

  @computed
  bool get isFormValid => login.length > 5 && password.length > 1;

  String validarLogin(String login) {
    if (login == null || login.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (login.length < 5) {
      return "O login precisa de no minimo de 5 caracteres";
    }

    return null;
  }

  String validarSenha(String senha) {
    if (senha == null || senha.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (senha.length < 5) {
      return "A senha precisa de no minimo de 5 caracteres";
    }

    return null;
  }

  String confirmarSenha(String senha) {
    if (senha == null || senha.isEmpty) {
      return "Este campo é obrigatorio";
    } else if (senha != this.password) {
      return "As senhas precisam ser iguais";
    }

    return null;
  }

  @computed
  Function get salvarPressed =>
      (isLoginValid && isPasswordValid) ? salvarLogin : null;

  //---------------------------------------------------------------------------

  @computed
  bool get isNomeChange => userFormValidate.nome != nomeO;

  @computed
  bool get isEmailChange => userFormValidate.email != emailO;

  @computed
  bool get isCrmChange => userFormValidate.crm != crmO;

  @computed
  bool get isCreChange => userFormValidate.cre != creO;

  @computed
  bool get change =>
      (isNomeChange || isEmailChange || isCrmChange || isCreChange);

  @action
  Future<void> proximo() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    if (teste == true) {
      try {
        if (change) {
          //Caso haja alguma alteração nos dados ele atualiza o registro no banco
          await admEditRepository.AlterarUsuario(
                  userFormValidate.id,
                  userFormValidate.nome,
                  userFormValidate.email,
                  userFormValidate.crm,
                  userFormValidate.cre)
              .then((value) {
            if (value.nome != null) {
              print("Dados atualizados");
              loading = false;
              Modular.to.pushNamed('/novologin');
              return;
            } else {
              appController.error_general = true;
              appController.msg = "Erro ao atualizar dados";
              return;
            }
          });
        } else {
          print("Sem alterações nos dados");
          Modular.to.pushNamed('/novologin');
          return;
        }
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao atualizar dados";
        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  Future<void> salvarLogin() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    if (teste == true) {
      loading = true;

      try {
        //-----------Ativar cadastro
        await newLoginRepository.ativar(userFormValidate.id).then((value) {
          if (value.nome != null) {
            print("ATIVADO");
          } else {
            appController.error_general = true;
            appController.msg = "Erro ao ativar cadastro";
            return;
          }
        });
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao ativar cadastro";
        return;
      }

      //-----------Cadastrar Login e Senha

      try {
        await newLoginRepository
            .novoLogin(userFormValidate.id, login, password)
            .then((value) async {
          if (value.login != null || value.login != "") {
            await limparController(); //Limpar userFormValidate
            await limparObservables(); //Limpar variaveis locais
            await limparOriginais(); //Limpar originais
            Modular.to.pushNamed("/");
            return;
          } else {
            appController.error_general = true;
            appController.msg = "Erro ao cadastrar login e senha";
            return;
          }
        });
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao cadastrar login e senha";
        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  @action
  Future<void> inativarUsuario() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    if (teste == true) {
      try {
        await admInativarRepository.Inativar(userFormValidate.id)
            .then((value) async {
          if (value.nome != null) {
            print(value.nome);
            await limparController();
            print("Inativado com sucesso");
            return;
          } else {
            appController.error_general = true;
            appController.msg = "Erro ao inativar usuario";
            return;
          }
        });
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao inativar usuario";
        return;
      }
    } else {
      appController.error_connect = true;
      return;
    }
  }

  //-----Scanner QR code

  var options = ScanOptions(
    autoEnableFlash: false,
    useCamera: -1, // default camera
    android: AndroidOptions(
      useAutoFocus: true,
    ),
  );

  @action
  Future<void> qrScanner() async {
    final a = await BarcodeScanner.scan(options: options);
    try {
      usuario = await qrRepository.qrscan(a.rawContent.toString());
      if (usuario.nome == null) {
        appController.error_general = true;
        appController.msg = "Erro ao recuperar dados";
      } else {
        print(usuario.id);
        userFormValidate.id = usuario.id;
        userFormValidate.nome = usuario.nome;
        userFormValidate.email = usuario.email;
        userFormValidate.crm = usuario.crm;
        userFormValidate.cre = usuario.cre;

        this.nomeO = usuario.nome;
        this.emailO = usuario.email;
        this.crmO = usuario.crm;
        this.creO = usuario.cre;

        if (usuario.cre == "" || usuario.cre == null) {
          userFormValidate.crmcheck = true;
          print('crm true nos dados');
          print(userFormValidate.crm);
        } else {
          userFormValidate.crecheck = true;
          print('cre true nos dados');
        }

        Modular.to.pushNamed(primeiroLogin);
      }
    } catch (e) {
      appController.error_general = true;
      appController.msg = "Erro ao recuperar dados";
    }
  }
}
