import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/models/user_model.dart';
import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:app_tcc/app/modules/shared/repositories/adm_repository.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
part 'adm_controller.g.dart';

class AdmController = _AdmControllerBase with _$AdmController;

abstract class _AdmControllerBase with Store {
  final appController = Modular.get<AppController>();

  final AdmRepository admRepository;

  @observable
  String search;

  @observable
  ObservableFuture<List<Usuario>> usuarios;

  @action
  void setSearch(String value) => search = value;

  @observable
  String nome = "";

  _AdmControllerBase(this.admRepository) {
    fetchUsuarios();
  }

  fetchUsuarios() {
    try {
      usuarios = admRepository.listarUsuarios().asObservable();
    } catch (e) {
      appController.error_general = true;
    }
  }

  getNomeShered() async {
    await LocalStorage.getValue<String>('nome').then((value) {
      nome = value;
      print("nome shered: " + nome);
    });
  }

  removeValuesShered() async {
    await LocalStorage.removeValue('id');
    await LocalStorage.removeValue('nome');
    await LocalStorage.removeValue('nivel');
    print("Removido shered");
  }

  addSearch() async {
    usuarios = admRepository.listarSearch(search).asObservable();
  }
}
