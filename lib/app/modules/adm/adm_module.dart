import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/adm/admnovo_controller.dart';
import 'package:app_tcc/app/modules/adm/pages/admnovo_page.dart';
import 'package:app_tcc/app/modules/shared/repositories/adm_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/admedit_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/adminativar_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/admnovo_email_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/admnovo_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/shared/widgets/newuser_form.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:app_tcc/app/modules/adm/admedit_controller.dart';
import 'package:app_tcc/app/modules/adm/pages/admedit_page.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';

import 'pages/adm_page.dart';

class AdmModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AdmEditController()),
        Bind((i) => AdmController(i.get())),
        Bind((i) => AdmRepository()),
        Bind((i) => AdmEditRepository()),
        Bind((i) => AdmInativarRepository()),
        Bind((i) => AdmNovoRepository()),
        Bind((i) => AdmNovoController()),
        Bind((i) => AdmNovoEmailRepository()),
        Bind((i) => ConnectValidate()),
        Bind((i) => UserFormValidate()),
        Bind((i) => NewUserForm())
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => AdmPage()),
        Router('/edit', child: (_, args) => AdmEditPage()),
        Router('/novo', child: (_, args) => AdmNovoPage())
      ];

  static Inject get to => Inject<AdmModule>.of();
}
