// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'admedit_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AdmEditController on _AdmEditControllerBase, Store {
  Computed<bool> _$isNomeChangeComputed;

  @override
  bool get isNomeChange =>
      (_$isNomeChangeComputed ??= Computed<bool>(() => super.isNomeChange,
              name: '_AdmEditControllerBase.isNomeChange'))
          .value;
  Computed<bool> _$isEmailChangeComputed;

  @override
  bool get isEmailChange =>
      (_$isEmailChangeComputed ??= Computed<bool>(() => super.isEmailChange,
              name: '_AdmEditControllerBase.isEmailChange'))
          .value;
  Computed<bool> _$isCrmChangeComputed;

  @override
  bool get isCrmChange =>
      (_$isCrmChangeComputed ??= Computed<bool>(() => super.isCrmChange,
              name: '_AdmEditControllerBase.isCrmChange'))
          .value;
  Computed<bool> _$isCreChangeComputed;

  @override
  bool get isCreChange =>
      (_$isCreChangeComputed ??= Computed<bool>(() => super.isCreChange,
              name: '_AdmEditControllerBase.isCreChange'))
          .value;
  Computed<Function> _$savePressedComputed;

  @override
  Function get savePressed =>
      (_$savePressedComputed ??= Computed<Function>(() => super.savePressed,
              name: '_AdmEditControllerBase.savePressed'))
          .value;

  final _$botaoAlterarAtom = Atom(name: '_AdmEditControllerBase.botaoAlterar');

  @override
  bool get botaoAlterar {
    _$botaoAlterarAtom.reportRead();
    return super.botaoAlterar;
  }

  @override
  set botaoAlterar(bool value) {
    _$botaoAlterarAtom.reportWrite(value, super.botaoAlterar, () {
      super.botaoAlterar = value;
    });
  }

  final _$botaoInativarAtom =
      Atom(name: '_AdmEditControllerBase.botaoInativar');

  @override
  bool get botaoInativar {
    _$botaoInativarAtom.reportRead();
    return super.botaoInativar;
  }

  @override
  set botaoInativar(bool value) {
    _$botaoInativarAtom.reportWrite(value, super.botaoInativar, () {
      super.botaoInativar = value;
    });
  }

  final _$idAtom = Atom(name: '_AdmEditControllerBase.id');

  @override
  int get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(int value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  final _$clicaBotaoAsyncAction =
      AsyncAction('_AdmEditControllerBase.clicaBotao');

  @override
  Future clicaBotao() {
    return _$clicaBotaoAsyncAction.run(() => super.clicaBotao());
  }

  final _$clicaBotaoInativarAsyncAction =
      AsyncAction('_AdmEditControllerBase.clicaBotaoInativar');

  @override
  Future clicaBotaoInativar() {
    return _$clicaBotaoInativarAsyncAction
        .run(() => super.clicaBotaoInativar());
  }

  final _$alterarAsyncAction = AsyncAction('_AdmEditControllerBase.alterar');

  @override
  Future alterar() {
    return _$alterarAsyncAction.run(() => super.alterar());
  }

  final _$inativarAsyncAction = AsyncAction('_AdmEditControllerBase.inativar');

  @override
  Future inativar() {
    return _$inativarAsyncAction.run(() => super.inativar());
  }

  final _$_AdmEditControllerBaseActionController =
      ActionController(name: '_AdmEditControllerBase');

  @override
  dynamic passartela(
      int id, String nome, String email, String crm, String cre) {
    final _$actionInfo = _$_AdmEditControllerBaseActionController.startAction(
        name: '_AdmEditControllerBase.passartela');
    try {
      return super.passartela(id, nome, email, crm, cre);
    } finally {
      _$_AdmEditControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
botaoAlterar: ${botaoAlterar},
botaoInativar: ${botaoInativar},
id: ${id},
isNomeChange: ${isNomeChange},
isEmailChange: ${isEmailChange},
isCrmChange: ${isCrmChange},
isCreChange: ${isCreChange},
savePressed: ${savePressed}
    ''';
  }
}
