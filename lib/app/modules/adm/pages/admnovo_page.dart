import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:app_tcc/app/modules/shared/widgets/newuser_form.dart';
import 'package:app_tcc/app/modules/shared/widgets/select_row.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

import '../admnovo_controller.dart';

class AdmNovoPage extends StatefulWidget {
  @override
  _AdmNovoPageState createState() => _AdmNovoPageState();
}

class _AdmNovoPageState extends State<AdmNovoPage> {
  static AdmNovoController admnovoController = new AdmNovoController();
  final appController = Modular.get<AppController>();
  static UserFormValidate userFormValidate = Modular.get<UserFormValidate>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    //erro de conexão
    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });

    reaction((_) => admnovoController.emailShared, (email) {
      if (email != "") {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(
                Icons.info,
                size: 40,
              ),
              content: Text(
                'Para sua segurança pedimos que o destinatario do email faça a verificação de autenticidade que chegara pelo endereço Amazon AWS ao email:  ${admnovoController.emailShared}.'
                "Somente após a verificação o usuario estará disponivel para o cadastro no sistema."
                "Caso o usuario ja tenha feito a verificação, clique em Proximo novamente",
                textAlign: TextAlign.justify,
              ),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("OK"),
                      onPressed: () {
                        admnovoController.emailShared = "";
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });
  }

  List<Step> steps = [
    Step(
        title: Text("Dados"),
        isActive: true,
        state: StepState.editing,
        content: NewUserForm()),
    Step(
      isActive: true,
      state: StepState.editing,
      title: const Text('Confirmação'),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Observer(
                builder: (_) {
                  return admnovoController.loadingNovoUsuario
                      ? CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.blue),
                        )
                      : Icon(
                          Icons.add,
                          color: Colors.green,
                          size: 40,
                        );
                },
              ),
              Observer(
                builder: (_) {
                  return admnovoController.loadingNovoUsuario
                      ? Text(
                          "Cadastrando dados !",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        )
                      : Text(
                          "Cadastrando dados !",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        );
                },
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Observer(
                builder: (_) {
                  return admnovoController.loadingMongoS3
                      ? CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.blue),
                        )
                      : Icon(
                          Icons.assignment_ind,
                          color: Colors.green,
                          size: 40,
                        );
                },
              ),
              Observer(
                builder: (_) {
                  return admnovoController.loadingMongoS3
                      ? Text(
                          "Salvando arquivo !",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        )
                      : Text(
                          "Salvando arquivo !",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        );
                },
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Observer(
                builder: (_) {
                  return admnovoController.loadingEmail
                      ? CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.blue),
                        )
                      : Icon(
                          Icons.email,
                          color: Colors.green,
                          size: 40,
                        );
                },
              ),
              Observer(
                builder: (_) {
                  return admnovoController.loadingMongoS3
                      ? Text(
                          "Enviando Email !",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        )
                      : Text(
                          "Enviado Email !",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        );
                },
              ),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
        ],
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
                title: admnovoController.step == 1
                    ? Text("Atenção ! os dados cadastrados serão perdidos")
                    : Text("Deseja retornar a tela principal ?"),
                actions: <Widget>[
                  Observer(
                    builder: (_) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          child: admnovoController.loading
                              ? CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.white),
                                )
                              : Text(
                                  "Sim",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                          color: Colors.green,
                          disabledColor:
                              Theme.of(context).primaryColor.withAlpha(100),
                          onPressed: admnovoController.anterior);
                    },
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32),
                    ),
                    child: Text(
                      "Não",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ));
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
          backgroundColor: Colors.white,
          title: Text(
            "Cadastro de usuario",
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(child: Observer(
              builder: (_) {
                return Stepper(
                  type: StepperType.horizontal,
                  steps: steps,
                  currentStep: admnovoController.step,
                  onStepContinue: admnovoController.proximo,
                  onStepCancel: admnovoController.anterior,
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue,
                      VoidCallback onStepCancel}) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Observer(
                          builder: (_) {
                            return SizedBox(
                              width: 130,
                              height: 50,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(32),
                                ),
                                child: admnovoController.loading
                                    ? CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            Colors.white),
                                      )
                                    : Text(
                                        "Proximo",
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                disabledColor: Theme.of(context)
                                    .primaryColor
                                    .withAlpha(100),
                                onPressed: admnovoController.proximoPressed,
                                color: Colors.green,
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          width: 130,
                          height: 50,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32),
                            ),
                            child: Text(
                              "Voltar",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            onPressed: onStepCancel,
                            color: Colors.blue,
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            )),
          ],
        ),
      ),
    );
  }
}
