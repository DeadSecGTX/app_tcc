import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/widgets/CardNovo.dart';
import 'package:flutter/material.dart';
import 'package:app_tcc/app/modules/shared/widgets/custom_text_field.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class AdmPage extends StatefulWidget {
  @override
  _AdmPageState createState() => _AdmPageState();
}

class _AdmPageState extends State<AdmPage> {
  final admController = Modular.get<AdmController>();
  final appController = Modular.get<AppController>();

  @override
  void initState() {
    super.initState();
    admController.getNomeShered();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // ignore: missing_return
    Future<bool> _onWillPop() async {
      await admController.removeValuesShered();
      Modular.to.pushNamed('/login/');
      await admController.fetchUsuarios();
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Observer(
        builder: (_) {
          return Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
              backgroundColor: Colors.white,
              title: Text(
                admController.nome,
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
              actions: <Widget>[
                //definir objetos de ação
                IconButton(
                  icon: Icon(
                    Icons.close,
                    size: 30,
                    color: Colors.red,
                  ),
                  onPressed: () async {
                    await admController.removeValuesShered();
                    Modular.to.pushNamed('/login/');
                    await admController.fetchUsuarios();
                  },
                )
              ],
            ),
            body: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  stops: [0.1, 1],
                  colors: [
                    Colors.blue[700],
                    Colors.lightBlue[100],
                  ],
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 10, left: 20, right: 10),
                          child: CustomTextField(
                            prefix: IconButton(
                                icon: Icon(Icons.search),
                                onPressed: admController.addSearch),
                            hint: 'Pesquise o nome...',
                            suffix: IconButton(
                                icon: Icon(Icons.clear),
                                onPressed: admController.fetchUsuarios),
                            textInputType: TextInputType.text,
                            onChanged: admController.setSearch,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                        child: ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: InkWell(
                              splashColor: Colors.green, // inkwell color
                              child: SizedBox(
                                  width: 56,
                                  height: 56,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.blue,
                                  )),
                              onTap: () {
                                Modular.to.pushNamed("/adm/novo");
                              },
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.all(5),
                        padding: const EdgeInsets.all(10),
                        child: Observer(
                          builder: (_) {
                            if (admController.usuarios.error != null) {
                              return Center(
                                child: FlatButton(
                                  color: Colors.green,
                                  onPressed: () {
                                    admController.fetchUsuarios();
                                  },
                                  child: Text('Recarregar'),
                                ),
                              );
                            }

                            if (admController.usuarios.value == null) {
                              return Center(child: CircularProgressIndicator());
                            }

                            var list = admController.usuarios.value;

                            return ListView.builder(
                              itemCount: admController.usuarios.value.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(16)),
                                  ),
                                  child: CardNovoWidget(
                                    id: list[index].id,
                                    nome: list[index].nome,
                                    email: list[index].email,
                                    crm: list[index].crm,
                                    cre: list[index].cre,
                                  ),
                                );
                              },
                            );
                          },
                        )),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
