import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/adm/admedit_controller.dart';
import 'package:app_tcc/app/modules/shared/responses/response_error.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class AdmEditPage extends StatefulWidget {
  @override
  _AdmEditPageState createState() => _AdmEditPageState();
}

class _AdmEditPageState extends State<AdmEditPage> {
  final admeditController = Modular.get<AdmEditController>();
  final appController = Modular.get<AppController>();
  final userFormValidate = Modular.get<UserFormValidate>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    //Erros diversos nos web services
    reaction((_) => appController.error_connect, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: "Ops! Não foi possivel conectar com a internet",
              funct: appController.limparConnect(),
            );
          },
        );
      }
    });

    reaction((_) => appController.error_general, (error) {
      if (error == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ResponseError(
              icon: Icons.warning,
              mensagem: appController.msg,
              funct: appController.limparGeneral(),
            );
          },
        );
      }
    });

    //Confirmar alteração
    reaction((_) => admeditController.botaoAlterar, (botaoAlterar) {
      if (botaoAlterar == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(Icons.warning),
              content: Text("Deseja alterar esses dados ?"),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await admeditController.alterar();
                        Navigator.pop(context);
                        Modular.to.pushNamed('/adm/');
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        admeditController.botaoAlterar = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });

    reaction((_) => admeditController.botaoInativar, (botaoInativar) {
      if (botaoInativar == true) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Icon(Icons.warning),
              content: Text("Deseja inativar esse usuario ?"),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () async {
                        await admeditController.inativar();
                        Navigator.pop(context);
                        Modular.to.pushNamed('/adm/');
                      },
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    RaisedButton(
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32),
                      ),
                      textColor: Colors.white,
                      child: Text("Cancelar"),
                      onPressed: () {
                        admeditController.botaoInativar = false;
                        Navigator.pop(context);
                      },
                    ),
                  ],
                )
              ],
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      await admeditController.limparControllerGlobal();
      Modular.to.pushNamed('/');
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.grey, opacity: 0.6),
            backgroundColor: Colors.white,
          ),
          body: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                //Nome
                Observer(
                  builder: (_) {
                    return TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Nome",
                        errorText:
                            userFormValidate.validarNome(userFormValidate.nome),
                      ),
                      initialValue: userFormValidate.nome,
                      onChanged: userFormValidate.setNome,
                    );
                  },
                ),
                const SizedBox(
                  height: 16,
                ),
                //EMAIL
                Observer(
                  builder: (_) {
                    return TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Email",
                        errorText: userFormValidate
                            .validarEmail(userFormValidate.email),
                      ),
                      initialValue: userFormValidate.email,
                      onChanged: userFormValidate.setEmail,
                    );
                  },
                ),
                const SizedBox(
                  height: 16,
                ),
                //CRM
                Observer(
                  builder: (_) {
                    return TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "CRM",
                        errorText:
                            userFormValidate.validarCrm(userFormValidate.crm),
                      ),
                      initialValue: userFormValidate.crm,
                      onChanged: userFormValidate.setCrm,
                    );
                  },
                ),
                const SizedBox(
                  height: 16,
                ),
                //CRE
                Observer(
                  builder: (_) {
                    return TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "CRE",
                        errorText:
                            userFormValidate.validarCre(userFormValidate.cre),
                      ),
                      initialValue: userFormValidate.cre,
                      onChanged: userFormValidate.setCre,
                    );
                  },
                ),
                const SizedBox(
                  height: 26,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Observer(
                      builder: (_) {
                        return SizedBox(
                          height: 50,
                          width: 150,
                          child: RaisedButton(
                            color: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32),
                            ),
                            disabledColor:
                                Theme.of(context).primaryColor.withAlpha(100),
                            textColor: Colors.white,
                            child: Text("Salvar"),
                            onPressed: admeditController.savePressed,
                          ),
                        );
                      },
                    ),
                    SizedBox(
                      height: 50,
                      width: 150,
                      child: RaisedButton(
                          color: Colors.redAccent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          textColor: Colors.white,
                          child: Text("Excluir"),
                          onPressed: () {
                            admeditController.clicaBotaoInativar();
                          }),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
