import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/shared/repositories/admedit_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/adminativar_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';

part 'admedit_controller.g.dart';

class AdmEditController = _AdmEditControllerBase with _$AdmEditController;

abstract class _AdmEditControllerBase with Store {
  final admController = Modular.get<AdmController>();
  final userFormValidate = Modular.get<UserFormValidate>();
  final admEditRepository = Modular.get<AdmEditRepository>();
  final admInativarRepository = Modular.get<AdmInativarRepository>();
  final appController = Modular.get<AppController>();

  limparControllerGlobal() {
    userFormValidate.nome = "";
    userFormValidate.email = "";
    userFormValidate.crm = "";
    userFormValidate.cre = "";
  }

  String nomeO, emailO, crmO, creO;

  @observable
  bool botaoAlterar = false;

  @observable
  bool botaoInativar = false;

  @observable
  int id = 0;

  @computed
  bool get isNomeChange => userFormValidate.nome != nomeO;

  @computed
  bool get isEmailChange => userFormValidate.email != emailO;

  @computed
  bool get isCrmChange => userFormValidate.crm != crmO;

  @computed
  bool get isCreChange => userFormValidate.cre != creO;

  @computed
  Function get savePressed =>
      (isNomeChange || isEmailChange || isCrmChange || isCreChange)
          ? clicaBotao
          : null;

  @action
  passartela(int id, String nome, String email, String crm, String cre) {
    userFormValidate.nome = nome;
    userFormValidate.email = email;
    userFormValidate.crm = crm;
    userFormValidate.cre = cre;

    this.id = id;
    this.nomeO = nome;
    this.emailO = email;
    this.crmO = crm;
    this.creO = cre;

    Modular.to.pushNamed('/edit');
  }

  @action
  clicaBotao() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoAlterar = true;
    }
  }

  @action
  clicaBotaoInativar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      botaoInativar = true;
    }
  }

  @action
  alterar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      try {
        await admEditRepository.AlterarUsuario(
                id,
                userFormValidate.nome,
                userFormValidate.email,
                userFormValidate.crm,
                userFormValidate.cre)
            .then((value) async {
          if (value.nome != null) {
            print(value.nome);
            botaoAlterar = false;
          } else {
            appController.error_general = true;
            appController.msg = "Erro ao alterar usuario";
            //await limparControllerGlobal();
            return;
          }
          await limparControllerGlobal();
          await admController.fetchUsuarios();
          botaoAlterar = false;
          return;
        });
      } catch (e) {
        //limparControllerGlobal();
        appController.error_general = true;
        appController.msg = "Erro ao alterar usuario";
        return;
      }
    } else {
      //limparControllerGlobal();
      appController.error_connect = true;
      return;
    }
  }

  @action
  inativar() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();
    if (teste == true) {
      try {
        await admInativarRepository.Inativar(id).then((value) async {
          if (value.nome != null) {
            print(value.nome);
            await admController.fetchUsuarios();
            botaoInativar = false;
          } else {
            //limparControllerGlobal();
            appController.error_general = true;
            appController.msg = "Erro ao inativar usuario";
            return;
          }
          await limparControllerGlobal();
          botaoAlterar = false;
          return;
        });
      } catch (e) {
        appController.error_general = true;
        appController.msg = "Erro ao inativar usuario";
        return;
      }
    } else {
      //limparControllerGlobal();
      appController.error_connect = true;
      return;
    }
  }
}
