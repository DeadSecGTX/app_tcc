// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'admnovo_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AdmNovoController on _AdmNovoControllerBase, Store {
  Computed<Function> _$proximoPressedComputed;

  @override
  Function get proximoPressed => (_$proximoPressedComputed ??=
          Computed<Function>(() => super.proximoPressed,
              name: '_AdmNovoControllerBase.proximoPressed'))
      .value;

  final _$emailSharedAtom = Atom(name: '_AdmNovoControllerBase.emailShared');

  @override
  String get emailShared {
    _$emailSharedAtom.reportRead();
    return super.emailShared;
  }

  @override
  set emailShared(String value) {
    _$emailSharedAtom.reportWrite(value, super.emailShared, () {
      super.emailShared = value;
    });
  }

  final _$id_usuarioAtom = Atom(name: '_AdmNovoControllerBase.id_usuario');

  @override
  int get id_usuario {
    _$id_usuarioAtom.reportRead();
    return super.id_usuario;
  }

  @override
  set id_usuario(int value) {
    _$id_usuarioAtom.reportWrite(value, super.id_usuario, () {
      super.id_usuario = value;
    });
  }

  final _$loadingAtom = Atom(name: '_AdmNovoControllerBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$loadingNovoUsuarioAtom =
      Atom(name: '_AdmNovoControllerBase.loadingNovoUsuario');

  @override
  bool get loadingNovoUsuario {
    _$loadingNovoUsuarioAtom.reportRead();
    return super.loadingNovoUsuario;
  }

  @override
  set loadingNovoUsuario(bool value) {
    _$loadingNovoUsuarioAtom.reportWrite(value, super.loadingNovoUsuario, () {
      super.loadingNovoUsuario = value;
    });
  }

  final _$loadingMongoS3Atom =
      Atom(name: '_AdmNovoControllerBase.loadingMongoS3');

  @override
  bool get loadingMongoS3 {
    _$loadingMongoS3Atom.reportRead();
    return super.loadingMongoS3;
  }

  @override
  set loadingMongoS3(bool value) {
    _$loadingMongoS3Atom.reportWrite(value, super.loadingMongoS3, () {
      super.loadingMongoS3 = value;
    });
  }

  final _$loadingEmailAtom = Atom(name: '_AdmNovoControllerBase.loadingEmail');

  @override
  bool get loadingEmail {
    _$loadingEmailAtom.reportRead();
    return super.loadingEmail;
  }

  @override
  set loadingEmail(bool value) {
    _$loadingEmailAtom.reportWrite(value, super.loadingEmail, () {
      super.loadingEmail = value;
    });
  }

  final _$stepAtom = Atom(name: '_AdmNovoControllerBase.step');

  @override
  int get step {
    _$stepAtom.reportRead();
    return super.step;
  }

  @override
  set step(int value) {
    _$stepAtom.reportWrite(value, super.step, () {
      super.step = value;
    });
  }

  final _$proximoAsyncAction = AsyncAction('_AdmNovoControllerBase.proximo');

  @override
  Future proximo() {
    return _$proximoAsyncAction.run(() => super.proximo());
  }

  final _$anteriorAsyncAction = AsyncAction('_AdmNovoControllerBase.anterior');

  @override
  Future anterior() {
    return _$anteriorAsyncAction.run(() => super.anterior());
  }

  @override
  String toString() {
    return '''
emailShared: ${emailShared},
id_usuario: ${id_usuario},
loading: ${loading},
loadingNovoUsuario: ${loadingNovoUsuario},
loadingMongoS3: ${loadingMongoS3},
loadingEmail: ${loadingEmail},
step: ${step},
proximoPressed: ${proximoPressed}
    ''';
  }
}
