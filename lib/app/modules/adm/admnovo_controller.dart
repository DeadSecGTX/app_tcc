import 'package:app_tcc/app/app_controller.dart';
import 'package:app_tcc/app/modules/adm/adm_controller.dart';
import 'package:app_tcc/app/modules/shared/repositories/admnovo_email_repository.dart';
import 'package:app_tcc/app/modules/shared/repositories/admnovo_repository.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:app_tcc/app/modules/shared/validate/user_form_validate.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:app_tcc/app/modules/shared/local_storage/local_storage.dart';
import 'package:mobx/mobx.dart';
part 'admnovo_controller.g.dart';

class AdmNovoController = _AdmNovoControllerBase with _$AdmNovoController;

abstract class _AdmNovoControllerBase with Store {
  final userFormValidate = Modular.get<UserFormValidate>();
  final admController = Modular.get<AdmController>();
  final admNovoRepository = Modular.get<AdmNovoRepository>();
  final admNovoEmailRepository = Modular.get<AdmNovoEmailRepository>();
  final appController = Modular.get<AppController>();

  _AdmNovoControllerBase() {
    autorun((_) {
      print("step esta em: " + step.toString());
    });
  }

  limparControllerGlobal() {
    userFormValidate.nome = "";
    userFormValidate.email = "";
    userFormValidate.crm = "";
    userFormValidate.cre = "";
  }

  String verify = "";

  Future<String> getVerifyShered() async {
    await LocalStorage.getValue<String>('emailVerify').then((value) {
      verify = value;
      print("verify: " + verify);
      return verify;
    });
  }

  removeEmailShered() async {
    await LocalStorage.removeValue('emailVerify');
    print("Removido email shered");
  }

  addEmailShered(String email) async {
    await LocalStorage.setValue<String>('emailVerify', email);
    emailShared = email;
  }

  @observable
  String emailShared = "";

  String key = "";

  String idMongo = "";

  String url = "";

  @observable
  int id_usuario = 0;

  @observable
  bool loading = false;

  @observable
  bool loadingNovoUsuario = false;

  @observable
  bool loadingMongoS3 = false;

  @observable
  bool loadingEmail = false;

  @observable
  int step = 0;

  @computed
  Function get proximoPressed =>
      (userFormValidate.crmSelect || userFormValidate.creSelect)
          ? proximo
          : null;

  @action
  proximo() async {
    var teste = await Modular.get<ConnectValidate>().isConnected();

    if (teste == true) {
      if (step == 0) {
        if (userFormValidate.crmcheck) // Se for médico
        {
          userFormValidate.cre = "";
        } else if (userFormValidate.crecheck) // Se for enfermeiro
        {
          userFormValidate.crm = "";
        }

        String ver = await getVerifyShered();
        print(ver);
        print(userFormValidate.email);
        if (ver == "" || ver == null) {
          bool ei =
              await admNovoEmailRepository.listaVerify(userFormValidate.email);
          if (ei) {
            loading = true;

            step++;

            loadingNovoUsuario = true;

            loadingMongoS3 = true;

            loadingEmail = true;

            try {
              await admNovoRepository.NovoUsuario(
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre,
                      1,
                      false)
                  .then((value) async {
                if (value.nome != null) {
                  id_usuario = value.id;

                  print("ID " + value.id.toString());

                  print("Nome " + value.nome);

                  loadingNovoUsuario = false;
                } else {
                  print("erro ao criar usuario");

                  loading = false;

                  loadingNovoUsuario = false;
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              appController.error_general = true;

              return;
            }

            try {
              await admNovoRepository
                  .salvarJson(
                      id_usuario,
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre)
                  .then((value) async {
                if (value.url != null) {
                  print(value.url);

                  key = value.key;

                  idMongo = value.id;

                  url = value.url;

                  print("Key " + key);

                  print("Id " + idMongo);

                  loadingMongoS3 = false;
                } else {
                  loading = false;

                  loadingMongoS3 = false;

                  print("erro ao salvar json");
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              await admNovoRepository.excluirProvisorio(id_usuario);

              appController.error_general = true;
              print("Erro ao salvar arquivo");

              return;
            }

            try {
              await admNovoRepository
                  .enviarEmail(userFormValidate.email, url)
                  .then((value) {
                if (value != null) {
                  print(value);

                  loading = false;

                  loadingEmail = false;
                } else {
                  loading = false;

                  loadingEmail = false;

                  print("erro ao enviar email");
                }
              });
            } catch (e) {
              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              step--;

              await admNovoRepository.excluirProvisorio(id_usuario);

              await admNovoRepository.excluirMongoS3(idMongo, key);

              appController.error_general = true;

              print("Erro ao enviar email");

              return;
            }

            loading = false;
          } else {
            loading = true;
            await addEmailShered(userFormValidate.email);
            bool testeVerefy =
                await admNovoEmailRepository.verify(userFormValidate.email);
            if (testeVerefy) {
              emailShared = userFormValidate.email;
              loading = false;
              return;
            } else {
              appController.error_general = true;
              print("Email invalido");
              loading = false;
              return;
            }
          }
        } else if (ver == userFormValidate.email) {
          bool ei = await admNovoEmailRepository.listaVerify(ver);
          if (ei) {
            await removeEmailShered();

            loading = true;

            step++;

            loadingNovoUsuario = true;

            loadingMongoS3 = true;

            loadingEmail = true;

            try {
              await admNovoRepository.NovoUsuario(
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre,
                      1,
                      false)
                  .then((value) async {
                if (value.nome != null) {
                  id_usuario = value.id;

                  print("ID " + value.id.toString());

                  print("Nome " + value.nome);

                  loadingNovoUsuario = false;
                } else {
                  print("erro ao criar usuario");

                  loading = false;

                  loadingNovoUsuario = false;
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              appController.error_general = true;
              print("Erro ao criar usuario");

              return;
            }

            try {
              await admNovoRepository
                  .salvarJson(
                      id_usuario,
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre)
                  .then((value) async {
                if (value.url != null) {
                  print(value.url);

                  key = value.key;

                  idMongo = value.id;

                  url = value.url;

                  print("Key " + key);

                  print("Id " + idMongo);

                  loadingMongoS3 = false;
                } else {
                  loading = false;

                  loadingMongoS3 = false;

                  print("erro ao salvar json");
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              await admNovoRepository.excluirProvisorio(id_usuario);

              appController.error_general = true;
              print("Erro ao salvar aquivo");

              return;
            }

            try {
              await admNovoRepository
                  .enviarEmail(userFormValidate.email, url)
                  .then((value) {
                if (value != null) {
                  print(value);

                  loading = false;

                  loadingEmail = false;
                } else {
                  loading = false;

                  loadingEmail = false;

                  print("erro ao enviar email");
                }
              });
            } catch (e) {
              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              step--;

              await admNovoRepository.excluirProvisorio(id_usuario);

              await admNovoRepository.excluirMongoS3(idMongo, key);

              appController.error_general = true;
              print("Erro ao enviar email");

              return;
            }

            loading = false;
          } else {
            bool testeVerefy =
                await admNovoEmailRepository.verify(userFormValidate.email);
            if (testeVerefy) {
              await addEmailShered(userFormValidate.email);
              emailShared = userFormValidate.email;
              return;
            } else {
              appController.error_general = true;
              print("Email invalido ao verificar");
              return;
            }
          }
        } else {
          bool ei =
              await admNovoEmailRepository.listaVerify(userFormValidate.email);
          if (ei) {
            loading = true;

            step++;

            loadingNovoUsuario = true;

            loadingMongoS3 = true;

            loadingEmail = true;

            try {
              await admNovoRepository.NovoUsuario(
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre,
                      1,
                      false)
                  .then((value) async {
                if (value.nome != null) {
                  id_usuario = value.id;

                  print("ID " + value.id.toString());

                  print("Nome " + value.nome);

                  loadingNovoUsuario = false;
                } else {
                  print("erro ao criar usuario");

                  loading = false;

                  loadingNovoUsuario = false;
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              appController.error_general = true;
              print("Erro ao criar usuario");

              return;
            }

            try {
              await admNovoRepository
                  .salvarJson(
                      id_usuario,
                      userFormValidate.nome,
                      userFormValidate.email,
                      userFormValidate.crm,
                      userFormValidate.cre)
                  .then((value) async {
                if (value.url != null) {
                  print(value.url);

                  key = value.key;

                  idMongo = value.id;

                  url = value.url;

                  print("Key " + key);

                  print("Id " + idMongo);

                  loadingMongoS3 = false;
                } else {
                  loading = false;

                  loadingMongoS3 = false;

                  print("erro ao salvar json");
                }
              });
            } catch (e) {
              step--;

              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              await admNovoRepository.excluirProvisorio(id_usuario);

              appController.error_general = true;
              print("Erro ao salvar arquivo");

              return;
            }

            try {
              await admNovoRepository
                  .enviarEmail(userFormValidate.email, url)
                  .then((value) {
                if (value != null) {
                  print(value);

                  loading = false;

                  loadingEmail = false;
                } else {
                  loading = false;

                  loadingEmail = false;

                  print("erro ao enviar email");
                }
              });
            } catch (e) {
              loading = false;

              loadingNovoUsuario = false;

              loadingMongoS3 = false;

              loadingEmail = false;

              step--;

              await admNovoRepository.excluirProvisorio(id_usuario);

              await admNovoRepository.excluirMongoS3(idMongo, key);

              appController.error_general = true;
              print("Erro ao enviar email");

              return;
            }

            loading = false;
          } else {
            await addEmailShered(userFormValidate.email);
            bool testeVerefy =
                await admNovoEmailRepository.verify(userFormValidate.email);
            if (testeVerefy) {
              emailShared = userFormValidate.email;
              return;
            } else {
              appController.error_general = true;
              print("Email invalido ao verificar");
              return;
            }
          }
        }
      } //Fim step == 0

      if (step == 1) {
        step = 0;
        userFormValidate.nome = "";
        userFormValidate.email = "";
        userFormValidate.crm = "";
        userFormValidate.cre = "";

        await admController.fetchUsuarios();

        Modular.to.pushNamed('/');
      }
    } else {
      print("Erro de conexão");
    }
  }

  @action
  anterior() async {
    if (step == 0) {
      step = 0;
      await limparControllerGlobal();
      admController.fetchUsuarios();
      Modular.to.pushNamed('/');
    }

    if (step == 1) {
      loading = true;

      try {
        await admNovoRepository.excluirProvisorio(id_usuario).then((value) {
          if (value != null) {
            print(value);
          } else {
            print("Erro ao excluir provisório");
          }
        });
      } catch (e) {
        appController.error_general = true;
        print("Erro ao excluir usuario");
      }

      try {
        await admNovoRepository.excluirMongoS3(idMongo, key).then((value) {
          if (value != null) {
            print(value);

            step--;

            loading = false;

            userFormValidate.nome = "";
            userFormValidate.email = "";
            userFormValidate.crm = "";
            userFormValidate.cre = "";

            //admController.fetchUsuarios();

            Modular.to.pushNamed('/adm/');
          } else {
            print("Erro ao excluir MongoS3");

            loading = false;
          }
        });
      } catch (e) {
        appController.error_general = true;
        print("Erro ao excluir arquivo");
      }
    }
  }
}
