// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'adm_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AdmController on _AdmControllerBase, Store {
  final _$searchAtom = Atom(name: '_AdmControllerBase.search');

  @override
  String get search {
    _$searchAtom.reportRead();
    return super.search;
  }

  @override
  set search(String value) {
    _$searchAtom.reportWrite(value, super.search, () {
      super.search = value;
    });
  }

  final _$usuariosAtom = Atom(name: '_AdmControllerBase.usuarios');

  @override
  ObservableFuture<List<Usuario>> get usuarios {
    _$usuariosAtom.reportRead();
    return super.usuarios;
  }

  @override
  set usuarios(ObservableFuture<List<Usuario>> value) {
    _$usuariosAtom.reportWrite(value, super.usuarios, () {
      super.usuarios = value;
    });
  }

  final _$nomeAtom = Atom(name: '_AdmControllerBase.nome');

  @override
  String get nome {
    _$nomeAtom.reportRead();
    return super.nome;
  }

  @override
  set nome(String value) {
    _$nomeAtom.reportWrite(value, super.nome, () {
      super.nome = value;
    });
  }

  final _$_AdmControllerBaseActionController =
      ActionController(name: '_AdmControllerBase');

  @override
  void setSearch(String value) {
    final _$actionInfo = _$_AdmControllerBaseActionController.startAction(
        name: '_AdmControllerBase.setSearch');
    try {
      return super.setSearch(value);
    } finally {
      _$_AdmControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
search: ${search},
usuarios: ${usuarios},
nome: ${nome}
    ''';
  }
}
