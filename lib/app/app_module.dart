import 'package:app_tcc/app/modules/Home/pages/home_meuspacientes_edit_page.dart';
import 'package:app_tcc/app/modules/Home/pages/home_page.dart';
import 'package:app_tcc/app/modules/adm/adm_module.dart';
import 'package:app_tcc/app/modules/adm/pages/admedit_page.dart';
import 'package:app_tcc/app/modules/adm/pages/admnovo_page.dart';
import 'package:app_tcc/app/modules/login/pages/login_novousuario_page.dart';
import 'package:app_tcc/app/modules/login/pages/novousuario_login.dart';
import 'package:app_tcc/app/modules/shared/utils/constants.dart';
import 'package:app_tcc/app/modules/shared/validate/connect_validate.dart';
import 'package:app_tcc/app/splash/splash_controller.dart';
import 'package:app_tcc/app/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'app_controller.dart';
import 'app_widget.dart';
import 'modules/Home/home_module.dart';
import 'modules/Home/pages/home_meuspacientes_page.dart';
import 'modules/login/login_controller.dart';
import 'modules/login/login_module.dart';
import 'modules/shared/repositories/login_repository.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => SplashController()),
        Bind((i) => LoginController()),
        Bind((i) => LoginRepository()),
        Bind((i) => ConnectValidate()),
      ];

  @override
  List<Router> get routers => [
        Router(splash, child: (_, args) => SplashPage()),
        Router(login, module: LoginModule()),
        Router(adm, module: AdmModule()),
        Router(admEditar, child: (_, args) => AdmEditPage()),
        Router(admNovo, child: (_, args) => AdmNovoPage()),
        Router(primeiroLogin, child: (_, args) => LoginNovoUsuarioPage()),
        Router('/novologin', child: (_, args) => NovoUsuarioLogin()),
        Router('/home', module: HomeModule()),
        Router('/meuspacientes', child: (_, args) => HomeMeusPacientes()),
        Router('/editpacientes', child: (_, args) => MeusPacientesEditPage())

        //Router('/meuspacientes', child: (_, args) => HomeMeusPacientes()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
