import 'package:mobx/mobx.dart';

part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  @observable
  bool error_connect = false;

  @observable
  bool error_general = false;

  @observable
  String msg = "";

  Future<void> limparGeneral() async {
    error_general = false;
    msg = "";
  }

  // ignore: missing_return
  Future<void> limparConnect() {
    error_connect = false;
  }
}
